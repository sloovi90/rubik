﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CubeFaceControl
{
    public partial class colors : Form
    {
        public int color { set; get; }
        public colors(PictureBox index)
        {
            InitializeComponent();
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            color = (int)CubeColors.RED;
            Hide();
        }

        private void colors_Load(object sender, EventArgs e)
        {

        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            color = (int)CubeColors.WHITE;
            Hide();
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            color = (int)CubeColors.ORANGE;
            Hide();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            color = (int)CubeColors.YELLOW;
            Hide();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            color = (int)CubeColors.GREEN;
            Hide();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            color = (int)CubeColors.BLUE;
            Hide();
        }
    }
}
