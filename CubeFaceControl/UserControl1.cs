﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CubeFaceControl
{
    public enum CubeColors { WHITE = 0, RED, ORANGE, BLUE, YELLOW, GREEN };
    public partial class UserControl1 : UserControl
    {
        List<PictureBox> boxes = new List<PictureBox>();
        public UserControl1()
        {
            InitializeComponent();
            boxes.Add(pictureBox1);
            boxes.Add(pictureBox2);
            boxes.Add(pictureBox3);
            boxes.Add(pictureBox4);
            boxes.Add(pictureBox5);
            boxes.Add(pictureBox6);
            boxes.Add(pictureBox7);
            boxes.Add(pictureBox8);
            boxes.Add(pictureBox9);


            foreach (PictureBox p in boxes) { 
                p.Tag = CubeColors.WHITE;
                p.Click += P_Click;
            }

        }

        private void P_Click(object sender, EventArgs e)
        {
            colors c = new colors((PictureBox)sender);
            c.ShowDialog();
            
            setColor((CubeColors)c.color, boxes.IndexOf((PictureBox)sender));
            
        }

        private void setColor(CubeColors color, int face)
        {
            switch (color)
            {
                case CubeColors.WHITE:
                    boxes[face].BackColor = Color.White;
                    break;
                case CubeColors.GREEN:
                    boxes[face].BackColor = Color.Green;
                    break;
                case CubeColors.RED:
                    boxes[face].BackColor = Color.Red;
                    break;

                case CubeColors.YELLOW:
                    boxes[face].BackColor = Color.Yellow;
                    break;
                case CubeColors.ORANGE:
                    boxes[face].BackColor = Color.Orange;
                    break;
                case CubeColors.BLUE:
                    boxes[face].BackColor = Color.Blue;
                    break;
            }
            boxes[face].Tag = color;
        }

        public void ResetSquares()
        {
            for (int i = 0; i < 9; i++)
                setColor(CubeColors.WHITE, i);
        }



        public CubeColors Face00
        {
            get
            {
                return (CubeColors)pictureBox1.Tag;
            }

            set
            {
                setColor(value, 0);
            }
        }
        public CubeColors Face01
        {
            get
            {
                return (CubeColors)pictureBox2.Tag;
            }

            set
            {
                setColor(value, 1);
            }
        }
        public CubeColors Face02
        {
            get
            {
                return (CubeColors)pictureBox3.Tag;
            }

            set
            {
                setColor(value, 2);
            }
        }
        public CubeColors Face10
        {
            get
            {
                return (CubeColors)pictureBox4.Tag;
            }

            set
            {
                setColor(value, 3);
            }
        }
        public CubeColors Face11
        {
            get
            {
                return (CubeColors)pictureBox5.Tag;
            }

            set
            {
                setColor(value, 4);
            }
        }
        public CubeColors Face12
        {
            get
            {
                return (CubeColors)pictureBox6.Tag;
            }

            set
            {
                setColor(value, 5);
            }
        }
        public CubeColors Face20
        {
            get
            {
                return (CubeColors)pictureBox7.Tag;
            }

            set
            {
                setColor(value, 6);
            }
        }
        public CubeColors Face21
        {
            get
            {
                return (CubeColors)pictureBox8.Tag;
            }

            set
            {
                setColor(value, 7);
            }
        }
        public CubeColors Face22
        {
            get
            {
                return (CubeColors)pictureBox9.Tag;
            }

            set
            {
                setColor(value, 8);
            }
        }

        private void UserControl1_Resize(object sender, EventArgs e)
        {
            this.Size = new Size(this.Width, this.Width);

            // Determine location
            pictureBox2.Location = new Point(Width/3,0);
            pictureBox3.Location = new Point(2*Width / 3, 0);
            pictureBox4.Location = new Point( 0, Width / 3);
            pictureBox5.Location = new Point(Width / 3, Width / 3);
            pictureBox6.Location = new Point(2*Width / 3, Width / 3);
            pictureBox7.Location = new Point(0,2* Width / 3);
            pictureBox8.Location = new Point(Width / 3,2* Width / 3);
            pictureBox9.Location = new Point(2*Width / 3, 2*Width / 3);

            // Determine size
            Size size = new Size(Width / 3, Height / 3);
            foreach (PictureBox pb in boxes)
                pb.Size = size;
        }
        public int[] getFaceArray()
        {
            int[] arr = { (int)Face00, (int)Face01, (int)Face02, (int)Face10, (int)Face11, (int)Face12, (int)Face20, (int)Face21, (int)Face22 };
            return arr;
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            //colors c = new colors(3);

        }
        //public CubeColors Face01
        //{
        //    get
        //    {
        //        return (CubeColors)pictureBox2.Tag;
        //    }

        //    set
        //    {
        //        switch (value)
        //        {
        //            case CubeColors.WHITE:
        //                pictureBox2.BackColor = Color.White;
        //                break;
        //            case CubeColors.GREEN:
        //                pictureBox2.BackColor = Color.Green;
        //                break;
        //            case CubeColors.RED:
        //                pictureBox2.BackColor = Color.Red;
        //                break;

        //            case CubeColors.YELLOW:
        //                pictureBox2.BackColor = Color.Yellow;
        //                break;

        //            case CubeColors.ORANGE:
        //                pictureBox2.BackColor = Color.Orange;
        //                break;


        //            case CubeColors.BLUE:
        //                pictureBox2.BackColor = Color.Blue;
        //                break;

        //        }
        //        pictureBox2.Tag = value;
        //    }
        //}



        //public CubeColors Face02
        //{
        //    get
        //    {
        //        return (CubeColors)pictureBox3.Tag;
        //    }

        //    set
        //    {
        //        switch (value)
        //        {
        //            case CubeColors.WHITE:
        //                pictureBox3.BackColor = Color.White;
        //                break;
        //            case CubeColors.GREEN:
        //                pictureBox3.BackColor = Color.Green;
        //                break;
        //            case CubeColors.RED:
        //                pictureBox3.BackColor = Color.Red;
        //                break;

        //            case CubeColors.YELLOW:
        //                pictureBox3.BackColor = Color.Yellow;
        //                break;

        //            case CubeColors.ORANGE:
        //                pictureBox3.BackColor = Color.Orange;
        //                break;


        //            case CubeColors.BLUE:
        //                pictureBox3.BackColor = Color.Blue;
        //                break;


        //        }
        //        pictureBox3.Tag = value;
        //    }
        //}



        //public CubeColors Face10
        //{
        //    get
        //    {
        //        return (CubeColors)pictureBox4.Tag;
        //    }

        //    set
        //    {
        //        switch (value)
        //        {
        //            case CubeColors.WHITE:
        //                pictureBox4.BackColor = Color.White;
        //                break;
        //            case CubeColors.GREEN:
        //                pictureBox4.BackColor = Color.Green;
        //                break;
        //            case CubeColors.RED:
        //                pictureBox4.BackColor = Color.Red;
        //                break;

        //            case CubeColors.YELLOW:
        //                pictureBox4.BackColor = Color.Yellow;
        //                break;

        //            case CubeColors.ORANGE:
        //                pictureBox4.BackColor = Color.Orange;
        //                break;


        //            case CubeColors.BLUE:
        //                pictureBox4.BackColor = Color.Blue;
        //                break;


        //        }
        //        pictureBox4.Tag = value;
        //    }
        //}

        //public CubeColors Face11
        //{
        //    get
        //    {
        //        return (CubeColors)pictureBox5.Tag;
        //    }

        //    set
        //    {
        //        switch (value)
        //        {
        //            case CubeColors.WHITE:
        //                pictureBox5.BackColor = Color.White;
        //                break;
        //            case CubeColors.GREEN:
        //                pictureBox5.BackColor = Color.Green;
        //                break;
        //            case CubeColors.RED:
        //                pictureBox5.BackColor = Color.Red;
        //                break;

        //            case CubeColors.YELLOW:
        //                pictureBox5.BackColor = Color.Yellow;
        //                break;

        //            case CubeColors.ORANGE:
        //                pictureBox5.BackColor = Color.Orange;
        //                break;


        //            case CubeColors.BLUE:
        //                pictureBox5.BackColor = Color.Blue;
        //                break;


        //        }
        //        pictureBox5.Tag = value;
        //    }
        //}

        //public CubeColors Face12
        //{
        //    get
        //    {
        //        return (CubeColors)pictureBox6.Tag;
        //    }

        //    set
        //    {
        //        switch (value)
        //        {
        //            case CubeColors.WHITE:
        //                pictureBox6.BackColor = Color.White;
        //                break;
        //            case CubeColors.GREEN:
        //                pictureBox6.BackColor = Color.Green;
        //                break;
        //            case CubeColors.RED:
        //                pictureBox6.BackColor = Color.Red;
        //                break;

        //            case CubeColors.YELLOW:
        //                pictureBox6.BackColor = Color.Yellow;
        //                break;

        //            case CubeColors.ORANGE:
        //                pictureBox6.BackColor = Color.Orange;
        //                break;


        //            case CubeColors.BLUE:
        //                pictureBox6.BackColor = Color.Blue;
        //                break;


        //        }
        //        pictureBox6.Tag = value;
        //    }
        //}


        //public CubeColors Face20
        //{
        //    get
        //    {
        //        return (CubeColors)pictureBox7.Tag;
        //    }

        //    set
        //    {
        //        switch (value)
        //        {
        //            case CubeColors.WHITE:
        //                pictureBox7.BackColor = Color.White;
        //                break;
        //            case CubeColors.GREEN:
        //                pictureBox7.BackColor = Color.Green;
        //                break;
        //            case CubeColors.RED:
        //                pictureBox7.BackColor = Color.Red;
        //                break;

        //            case CubeColors.YELLOW:
        //                pictureBox7.BackColor = Color.Yellow;
        //                break;

        //            case CubeColors.ORANGE:
        //                pictureBox7.BackColor = Color.Orange;
        //                break;


        //            case CubeColors.BLUE:
        //                pictureBox7.BackColor = Color.Blue;
        //                break;


        //        }
        //        pictureBox7.Tag = value;
        //    }
        //}

        //public CubeColors Face21
        //{
        //    get
        //    {
        //        return (CubeColors)pictureBox8.Tag;
        //    }

        //    set
        //    {
        //        switch (value)
        //        {
        //            case CubeColors.WHITE:
        //                pictureBox8.BackColor = Color.White;
        //                break;
        //            case CubeColors.GREEN:
        //                pictureBox8.BackColor = Color.Green;
        //                break;
        //            case CubeColors.RED:
        //                pictureBox8.BackColor = Color.Red;
        //                break;

        //            case CubeColors.YELLOW:
        //                pictureBox8.BackColor = Color.Yellow;
        //                break;

        //            case CubeColors.ORANGE:
        //                pictureBox8.BackColor = Color.Orange;
        //                break;


        //            case CubeColors.BLUE:
        //                pictureBox8.BackColor = Color.Blue;
        //                break;


        //        }
        //        pictureBox8.Tag = value;
        //    }
        //}

        //public CubeColors Face22
        //{
        //    get
        //    {
        //        return (CubeColors)pictureBox9.Tag;
        //    }

        //    set
        //    {
        //        switch (value)
        //        {
        //            case CubeColors.WHITE:
        //                pictureBox9.BackColor = Color.White;
        //                break;
        //            case CubeColors.GREEN:
        //                pictureBox9.BackColor = Color.Green;
        //                break;
        //            case CubeColors.RED:
        //                pictureBox9.BackColor = Color.Red;
        //                break;

        //            case CubeColors.YELLOW:
        //                pictureBox9.BackColor = Color.Yellow;
        //                break;

        //            case CubeColors.ORANGE:
        //                pictureBox9.BackColor = Color.Orange;
        //                break;


        //            case CubeColors.BLUE:
        //                pictureBox9.BackColor = Color.Blue;
        //                break;


        //        }
        //        pictureBox9.Tag = value;
        //    }
        //}



    }
}
