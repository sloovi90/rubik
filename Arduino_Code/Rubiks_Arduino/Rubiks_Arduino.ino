 /*
 * List of available commands:
 * ===================================================
 *  'A': Rotate the base 90 degrees clockwise
 *  'a': Rotate the base 90 degrees counterclockwise
 * 
 *  'Z': Rotate the base 180 degrees clockwise
 * 
 *  'C': Rotate the side servos forwards (top -> front)
 *  'c': Rotate the side servos backwards (front -> top)
 *  
 *  'F': Rotate the side servos 180 degress (assuming the cube platform is already elevated!)
 * 
 *  'U': Elevate the cube platform
 *  'u': Lower the cube platform
 * 
 *  'P': Push the side servos into the cube
 *  'p': Push the side servos away from the cube
 * 
 *  'L': Turn the LED on
 *  'l': Turn the LED off
 * 
 *  'S': Turns the stepper 1/8 step clockwise
 *  's': Turns the stepper 1/8 step counterclockwise
 *  'T': Turns the stepper 1 step clockwise
 *  't': Turns the stepper 1 step counterclockwise
 *  
 *  'r': Reset the system
 *  'f': Clear the Bluetooth incoming byte stream
 */

#include <Servo.h>
#include <Stepper.h>
#include <Encoder.h>

/*************** Stepper definitions ***************/
#define STEPPER_DELAY_BETWEEN_STEPS   1500            // Stepper speed; lower is faster, usually 500 is the safe minimum
#define STEPPER_STEPS_PER_REVOLUTION  200             // Determined by our stepper model
#define STEPPER_MICROSTEPPING_FACTOR  8               // Our stepper uses 1/8 microstepping

#define STEPPER_STEP_PIN              11
#define STEPPER_DIR_PIN               12

/*************** DC motor definitions ***************/
// NOTE: Since the DC motor is not very accurate, we run it in full speed up to the last MOTOR_LAND_ROTATIONS rotations,
//       which are then run in a lesser speed, MOTOR_LAND_SPEED. This reminds an elevator that moves fast, and then moves
//       very slow to ensure the elevator opens exactly at floor level.
//       During system reset, the DC motor is spun at MOTOR_LAND_SPEED until the endstop is triggered

#define ENCODER_CPR                   1200L           // The number of quadrature pulses created by the encoder for each revolution
#define MOTOR_SHAFT_LOW_POINT         0               // Value of the lowest height of the screw shaft
#define MOTOR_SHAFT_HIGH_POINT        24L*ENCODER_CPR // Value of the greatest height of the screw shaft 
#define MOTOR_RUN_SPEED               240L            // Value in range [0..255]; this is the speed in which the motor runs
#define MOTOR_LAND_SPEED              120L            // Value in range [0..255]; this is the speed in which the last few rotations are made, to ensure accuracy
#define MOTOR_LAND_ROTATIONS          4L              // The number of rotations the motor will perform in land speed before stopping completely

#define MOTOR_ENCODER_A_PIN           2
#define MOTOR_ENCODER_B_PIN           3
#define MOTOR_IN1_PIN                 4
#define MOTOR_IN2_PIN                 5
#define MOTOR_ENABLE_PIN              6

/*************** Servo definitions ***************/
// Servo position values
#define RIGHT_ROTATOR_START           115
#define RIGHT_ROTATOR_END             10
#define LEFT_ROTATOR_START            15
#define LEFT_ROTATOR_END              110
#define RIGHT_PUSHER_START            40
#define RIGHT_PUSHER_END              130
#define LEFT_PUSHER_START             130
#define LEFT_PUSHER_END               30

#define RIGHT_ROTATOR_PIN             7
#define LEFT_ROTATOR_PIN              8
#define RIGHT_PUSHER_PIN              9
#define LEFT_PUSHER_PIN               10

/*************** Other definitions ***************/
#define FLASH_LED_PIN                 13
#define DEBUG_DELAY_AFTER_COMMANDS    0            // A delay to be added after every command, in ms. For debug purposes


/*************** Type declarations ***************/
Servo    rightRotator, leftRotator,
         rightPusher, leftPusher
         ;
Encoder  baseMotor(MOTOR_ENCODER_A_PIN, MOTOR_ENCODER_B_PIN);


void setup()
{
  // Initialize Bluetooth / general serial channel
  Serial.begin(9600);

  resetSystem();
}

char command = -1;

void loop()
{
  delay(100);
  
  if(!Serial.available())
    return;
    
  command = Serial.read();
  if(command == -1)
    return;

  executeCommand(command, true);

  Serial.print('D');    // Write an acknowledgement back

}

// Set useDelay=true to wait until the command finishes (in the physical world) or false to return immediately
void executeCommand(char command, bool useDelay) {
  char a, b;
  int power;
  switch(command) {
    case 'A':
      moveStepper(STEPPER_STEPS_PER_REVOLUTION / 4 * STEPPER_MICROSTEPPING_FACTOR, HIGH, areServosPushed());
      break;
    case 'a':
      moveStepper(STEPPER_STEPS_PER_REVOLUTION / 4 * STEPPER_MICROSTEPPING_FACTOR, LOW, areServosPushed());
      break;
    case 'Z':
      moveStepper(STEPPER_STEPS_PER_REVOLUTION / 2 * STEPPER_MICROSTEPPING_FACTOR, LOW, areServosPushed());
      break;
    case 'C':
      rightRotator.write(RIGHT_ROTATOR_END);
      leftRotator.write(LEFT_ROTATOR_END);
      delay(300);
      break;
    case 'c':
      rightRotator.write(RIGHT_ROTATOR_START);
      leftRotator.write(LEFT_ROTATOR_START);
      delay(300);
      break;
    case 'U':
      runMotor(MOTOR_SHAFT_HIGH_POINT);
      delay(100);
      break;
    case 'u':
      runMotor(MOTOR_SHAFT_LOW_POINT);
      delay(100);
      break;
    case 'P':
      rightPusher.write(RIGHT_PUSHER_END);
      leftPusher.write(LEFT_PUSHER_END);
      delay(300);
      break;
    case 'p':
      rightPusher.write(RIGHT_PUSHER_START);
      leftPusher.write(LEFT_PUSHER_START);
      delay(300);
      break;
    case 'F':
      executeCommand('P', true);
      executeCommand('u', true);
      executeCommand('C', true);
      executeCommand('U', true);
      executeCommand('p', false);
      executeCommand('c', true);
      executeCommand('P', true);
      executeCommand('u', true);
      executeCommand('C', true);
      executeCommand('U', true);
      executeCommand('p', false);
      executeCommand('c', true);
      break;
    case 'L':
      powerLED(true);
      break;
    case 'l':
      powerLED(false);
      break;
    case 'S':
      moveStepper(1, HIGH, false);
      break;
    case 's':
      moveStepper(1, LOW, false);
      break;
    case 'T':
      moveStepper(STEPPER_MICROSTEPPING_FACTOR, HIGH, false);
      break;
    case 't':
      moveStepper(STEPPER_MICROSTEPPING_FACTOR, LOW, false);
      break;
    case 'r':
      resetSystem();
      break;
    case 'f':
      while(Serial.available()) {
        Serial.read();
      }
      break;
    case 'd':
      runDemo();
      break;
  }
  delay(DEBUG_DELAY_AFTER_COMMANDS);
}

// Moves the stepper the required number of steps. If the stepper driver uses microstepping,
// you should multiply numOfSteps by the microstepping factor.
void moveStepper(int numOfSteps, int dir, boolean fixCubeDeviation) {
  digitalWrite(STEPPER_DIR_PIN, dir);
  delay(10);
  
  for(int i = 0 ; i < numOfSteps ; i++) {
    digitalWrite(STEPPER_STEP_PIN, LOW);
    digitalWrite(STEPPER_STEP_PIN, HIGH);
    delayMicroseconds(STEPPER_DELAY_BETWEEN_STEPS);
  }

  delay(50);


  // When turning one of the faces, a small deviation may occur due to the spacing in
  // the base. When calling this function to rotate a face, this block fixes this deviation
  // by running the stepper a little futher and back
  if(fixCubeDeviation) {
    moveStepper(4 * STEPPER_MICROSTEPPING_FACTOR, dir, false);
    moveStepper(4 * STEPPER_MICROSTEPPING_FACTOR, !dir, false);
    delay(50);
  }
}

// Runs the motor to elevate or lower the cube base. It uses the encoder's stored value to
// count rotations, and uses the endstop on analog input A0 as contingency
void runMotor(long pos) {
  if(baseMotor.read() == pos)
    return;

  analogWrite(MOTOR_ENABLE_PIN, MOTOR_RUN_SPEED);
  
  if(baseMotor.read() < pos) {
    // Elevate the base of the cube
    digitalWrite(MOTOR_IN1_PIN, HIGH);
    digitalWrite(MOTOR_IN2_PIN, LOW);

    // Rotate in run speed
    while(baseMotor.read() < pos - ENCODER_CPR*MOTOR_LAND_ROTATIONS) {}

    // Rotate the last rounds in land speed
    analogWrite(MOTOR_ENABLE_PIN, MOTOR_LAND_SPEED);
    while(baseMotor.read() < pos) {}
  } else {
    // Lower the base of the cube
    digitalWrite(MOTOR_IN1_PIN, LOW);
    digitalWrite(MOTOR_IN2_PIN, HIGH);

    // Rotate in run speed
    while(!isStopPressed() && baseMotor.read() > pos + ENCODER_CPR*MOTOR_LAND_ROTATIONS) {};

    // Rotate the last rounds in land speed
    analogWrite(MOTOR_ENABLE_PIN, MOTOR_LAND_SPEED);
    while(!isStopPressed() && baseMotor.read() > pos) {}
  }

  analogWrite(MOTOR_ENABLE_PIN, LOW);
  delay(1000);
}

// Resets the DC motor - lowers the cube base slowly until the endstop is pressed,
// then resets the encoder value to 0
void resetMotor() {
  analogWrite(MOTOR_ENABLE_PIN, MOTOR_LAND_SPEED);
  digitalWrite(MOTOR_IN1_PIN, LOW);
  digitalWrite(MOTOR_IN2_PIN, HIGH);

  while(!isStopPressed());
  analogWrite(MOTOR_ENABLE_PIN, LOW);

  delay(200);

  baseMotor.write(-200);
}

boolean isStopPressed() {
  return analogRead(A0) < 512;
}

boolean areServosPushed() {
  return rightPusher.read() == RIGHT_PUSHER_END;
}

void powerLED(bool on) {
  digitalWrite(FLASH_LED_PIN, on);
}

void resetSystem() {
  // Initialize servos
  rightRotator.attach(RIGHT_ROTATOR_PIN);
  leftRotator.attach(LEFT_ROTATOR_PIN);
  rightPusher.attach(RIGHT_PUSHER_PIN);
  leftPusher.attach(LEFT_PUSHER_PIN);

  rightRotator.write(RIGHT_ROTATOR_START);
  leftRotator.write(LEFT_ROTATOR_START);
  rightPusher.write(RIGHT_PUSHER_START);
  leftPusher.write(LEFT_PUSHER_START);

  delay(500);

  // Initialize stepper
  pinMode(STEPPER_STEP_PIN, OUTPUT);
  pinMode(STEPPER_DIR_PIN, OUTPUT);

  // Initialize LED
  pinMode(FLASH_LED_PIN, OUTPUT);

  // Move the rotators back and forth, for some reason it calibrates them
  executeCommand('C', true);
  executeCommand('c', false);

  // Reset the motor to its starting position
  resetMotor();

  delay(500);
}

void runDemo() {
  String solution = "AUPuCUpcPapCPucUAp";

  int i;
  for(i = 0 ; i < 18 ; i++)
    executeCommand(solution.charAt(i), true);

  
  

  /*
  executeCommand('u', true);
  delay(1000);

  executeCommand('A', true);
  delay(1000);
  
  executeCommand('A', true);
  delay(1000);

  executeCommand('A', true);
  delay(1000);

  executeCommand('U', true);
  delay(1000);

  executeCommand('P', true);
  delay(1000);

  executeCommand('u', true);
  delay(1000);

  executeCommand('C', true);
  delay(1000);

  executeCommand('U', true);
  delay(1000);

  executeCommand('p', true);
  delay(50);
  executeCommand('c', true);
  delay(1000);

  executeCommand('u', true);
  delay(1000);
  
  executeCommand('A', true);
  delay(1000);

  executeCommand('U', true);
  delay(1000);

  executeCommand('P', true);
  delay(1000);

  executeCommand('Z', true);
  delay(1500);

  executeCommand('p', true);
  delay(1000);
  */
  /*executeCommand('A', true);
  executeCommand('A', true);
  executeCommand('A', true);
  executeCommand('a', true);
  executeCommand('a', true);
  executeCommand('a', true);
  executeCommand('Z', true);
  executeCommand('Z', true);
  */
  

  /*
  executeCommand('A');
  delay(1000);
  executeCommand('A');
  delay(1000);

  executeCommand('P');
  delay(1000);

  executeCommand('u');
  delay(1000);

  executeCommand('c');
  delay(1000);

  executeCommand('U');
  delay(1000);

  executeCommand('p');
  delay(1000);
    
  executeCommand('u');
  delay(2000);*/
}

