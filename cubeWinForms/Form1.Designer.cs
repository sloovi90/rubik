﻿
namespace cubeWinForms
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.imgVideoFeed = new Emgu.CV.UI.ImageBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.imgMarker9 = new System.Windows.Forms.PictureBox();
            this.imgMarker5 = new System.Windows.Forms.PictureBox();
            this.imgMarker8 = new System.Windows.Forms.PictureBox();
            this.imgMarker1 = new System.Windows.Forms.PictureBox();
            this.imgMarker2 = new System.Windows.Forms.PictureBox();
            this.imgMarker7 = new System.Windows.Forms.PictureBox();
            this.imgMarker3 = new System.Windows.Forms.PictureBox();
            this.imgMarker4 = new System.Windows.Forms.PictureBox();
            this.imgMarker6 = new System.Windows.Forms.PictureBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.btnSolve = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.calibrationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.systemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.colorsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.calibrateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearModelDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.actionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cubeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.elevateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lowerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cubeRotationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rotate90CWToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rotate90CCWToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rotate180ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.platformsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pushIntoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pullAwayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rotateTo90ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rotateTo0ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lEDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.turnOnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.turnOffToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cameraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.devicesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.captureSideToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tmrRefreshCaptureDevices = new System.Windows.Forms.Timer(this.components);
            this.cfcBottom = new CubeFaceControl.UserControl1();
            this.cfcLeft = new CubeFaceControl.UserControl1();
            this.cfcBack = new CubeFaceControl.UserControl1();
            this.cfcRight = new CubeFaceControl.UserControl1();
            this.cfcTop = new CubeFaceControl.UserControl1();
            this.cfcFront = new CubeFaceControl.UserControl1();
            this.label2 = new System.Windows.Forms.Label();
            this.lblRemainingTime = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.lblRemainingMoves = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.lblCurrentMove = new System.Windows.Forms.Label();
            this.tmrTimeToSolution = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.imgVideoFeed)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgMarker9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMarker5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMarker8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMarker1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMarker2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMarker7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMarker3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMarker4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMarker6)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // imgVideoFeed
            // 
            this.imgVideoFeed.Location = new System.Drawing.Point(0, 0);
            this.imgVideoFeed.Name = "imgVideoFeed";
            this.imgVideoFeed.Size = new System.Drawing.Size(400, 400);
            this.imgVideoFeed.TabIndex = 2;
            this.imgVideoFeed.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Location = new System.Drawing.Point(12, 33);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(424, 432);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Live feed";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.Controls.Add(this.imgMarker9);
            this.panel1.Controls.Add(this.imgMarker5);
            this.panel1.Controls.Add(this.imgMarker8);
            this.panel1.Controls.Add(this.imgMarker1);
            this.panel1.Controls.Add(this.imgMarker2);
            this.panel1.Controls.Add(this.imgMarker7);
            this.panel1.Controls.Add(this.imgMarker3);
            this.panel1.Controls.Add(this.imgMarker4);
            this.panel1.Controls.Add(this.imgMarker6);
            this.panel1.Controls.Add(this.imgVideoFeed);
            this.panel1.Location = new System.Drawing.Point(11, 18);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(400, 400);
            this.panel1.TabIndex = 16;
            // 
            // imgMarker9
            // 
            this.imgMarker9.BackColor = System.Drawing.Color.Transparent;
            this.imgMarker9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imgMarker9.Location = new System.Drawing.Point(298, 298);
            this.imgMarker9.Name = "imgMarker9";
            this.imgMarker9.Size = new System.Drawing.Size(12, 12);
            this.imgMarker9.TabIndex = 11;
            this.imgMarker9.TabStop = false;
            // 
            // imgMarker5
            // 
            this.imgMarker5.BackColor = System.Drawing.Color.Transparent;
            this.imgMarker5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imgMarker5.Location = new System.Drawing.Point(198, 198);
            this.imgMarker5.Name = "imgMarker5";
            this.imgMarker5.Size = new System.Drawing.Size(12, 12);
            this.imgMarker5.TabIndex = 7;
            this.imgMarker5.TabStop = false;
            // 
            // imgMarker8
            // 
            this.imgMarker8.BackColor = System.Drawing.Color.Transparent;
            this.imgMarker8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imgMarker8.Location = new System.Drawing.Point(198, 298);
            this.imgMarker8.Name = "imgMarker8";
            this.imgMarker8.Size = new System.Drawing.Size(12, 12);
            this.imgMarker8.TabIndex = 10;
            this.imgMarker8.TabStop = false;
            // 
            // imgMarker1
            // 
            this.imgMarker1.BackColor = System.Drawing.Color.Transparent;
            this.imgMarker1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imgMarker1.Location = new System.Drawing.Point(98, 98);
            this.imgMarker1.Name = "imgMarker1";
            this.imgMarker1.Size = new System.Drawing.Size(12, 12);
            this.imgMarker1.TabIndex = 3;
            this.imgMarker1.TabStop = false;
            // 
            // imgMarker2
            // 
            this.imgMarker2.BackColor = System.Drawing.Color.Transparent;
            this.imgMarker2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imgMarker2.Location = new System.Drawing.Point(198, 98);
            this.imgMarker2.Name = "imgMarker2";
            this.imgMarker2.Size = new System.Drawing.Size(12, 12);
            this.imgMarker2.TabIndex = 4;
            this.imgMarker2.TabStop = false;
            // 
            // imgMarker7
            // 
            this.imgMarker7.BackColor = System.Drawing.Color.Transparent;
            this.imgMarker7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imgMarker7.Location = new System.Drawing.Point(98, 298);
            this.imgMarker7.Name = "imgMarker7";
            this.imgMarker7.Size = new System.Drawing.Size(12, 12);
            this.imgMarker7.TabIndex = 9;
            this.imgMarker7.TabStop = false;
            // 
            // imgMarker3
            // 
            this.imgMarker3.BackColor = System.Drawing.Color.Transparent;
            this.imgMarker3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imgMarker3.Location = new System.Drawing.Point(298, 98);
            this.imgMarker3.Name = "imgMarker3";
            this.imgMarker3.Size = new System.Drawing.Size(12, 12);
            this.imgMarker3.TabIndex = 5;
            this.imgMarker3.TabStop = false;
            // 
            // imgMarker4
            // 
            this.imgMarker4.BackColor = System.Drawing.Color.Transparent;
            this.imgMarker4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imgMarker4.Location = new System.Drawing.Point(98, 198);
            this.imgMarker4.Name = "imgMarker4";
            this.imgMarker4.Size = new System.Drawing.Size(12, 12);
            this.imgMarker4.TabIndex = 6;
            this.imgMarker4.TabStop = false;
            // 
            // imgMarker6
            // 
            this.imgMarker6.BackColor = System.Drawing.Color.Transparent;
            this.imgMarker6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imgMarker6.Location = new System.Drawing.Point(298, 198);
            this.imgMarker6.Name = "imgMarker6";
            this.imgMarker6.Size = new System.Drawing.Size(12, 12);
            this.imgMarker6.TabIndex = 8;
            this.imgMarker6.TabStop = false;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // btnSolve
            // 
            this.btnSolve.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnSolve.Font = new System.Drawing.Font("Tahoma", 18F);
            this.btnSolve.Location = new System.Drawing.Point(12, 471);
            this.btnSolve.Name = "btnSolve";
            this.btnSolve.Size = new System.Drawing.Size(424, 46);
            this.btnSolve.TabIndex = 0;
            this.btnSolve.Text = "Solve";
            this.btnSolve.UseVisualStyleBackColor = true;
            this.btnSolve.Click += new System.EventHandler(this.button3_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.calibrationsToolStripMenuItem,
            this.actionsToolStripMenuItem,
            this.cameraToolStripMenuItem,
            this.quitToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1612, 24);
            this.menuStrip1.TabIndex = 19;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // calibrationsToolStripMenuItem
            // 
            this.calibrationsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.systemToolStripMenuItem,
            this.colorsToolStripMenuItem1});
            this.calibrationsToolStripMenuItem.Name = "calibrationsToolStripMenuItem";
            this.calibrationsToolStripMenuItem.Size = new System.Drawing.Size(82, 20);
            this.calibrationsToolStripMenuItem.Text = "Calibrations";
            // 
            // systemToolStripMenuItem
            // 
            this.systemToolStripMenuItem.Name = "systemToolStripMenuItem";
            this.systemToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.systemToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.systemToolStripMenuItem.Text = "System";
            this.systemToolStripMenuItem.Click += new System.EventHandler(this.systemToolStripMenuItem_Click);
            // 
            // colorsToolStripMenuItem1
            // 
            this.colorsToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.calibrateToolStripMenuItem,
            this.clearModelDataToolStripMenuItem});
            this.colorsToolStripMenuItem1.Name = "colorsToolStripMenuItem1";
            this.colorsToolStripMenuItem1.Size = new System.Drawing.Size(184, 22);
            this.colorsToolStripMenuItem1.Text = "Colors";
            // 
            // calibrateToolStripMenuItem
            // 
            this.calibrateToolStripMenuItem.Name = "calibrateToolStripMenuItem";
            this.calibrateToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.C)));
            this.calibrateToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.calibrateToolStripMenuItem.Text = "Calibrate...";
            this.calibrateToolStripMenuItem.Click += new System.EventHandler(this.calibrateToolStripMenuItem_Click);
            // 
            // clearModelDataToolStripMenuItem
            // 
            this.clearModelDataToolStripMenuItem.Name = "clearModelDataToolStripMenuItem";
            this.clearModelDataToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.clearModelDataToolStripMenuItem.Text = "Clear Model Data";
            this.clearModelDataToolStripMenuItem.Click += new System.EventHandler(this.clearModelDataToolStripMenuItem_Click);
            // 
            // actionsToolStripMenuItem
            // 
            this.actionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cubeToolStripMenuItem,
            this.platformsToolStripMenuItem,
            this.lEDToolStripMenuItem});
            this.actionsToolStripMenuItem.Name = "actionsToolStripMenuItem";
            this.actionsToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.actionsToolStripMenuItem.Text = "Actions";
            // 
            // cubeToolStripMenuItem
            // 
            this.cubeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.elevateToolStripMenuItem,
            this.lowerToolStripMenuItem,
            this.cubeRotationToolStripMenuItem});
            this.cubeToolStripMenuItem.Name = "cubeToolStripMenuItem";
            this.cubeToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.cubeToolStripMenuItem.Text = "Cube";
            // 
            // elevateToolStripMenuItem
            // 
            this.elevateToolStripMenuItem.Name = "elevateToolStripMenuItem";
            this.elevateToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.elevateToolStripMenuItem.Text = "Elevate";
            this.elevateToolStripMenuItem.Click += new System.EventHandler(this.elevateToolStripMenuItem_Click);
            // 
            // lowerToolStripMenuItem
            // 
            this.lowerToolStripMenuItem.Name = "lowerToolStripMenuItem";
            this.lowerToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.lowerToolStripMenuItem.Text = "Lower";
            this.lowerToolStripMenuItem.Click += new System.EventHandler(this.lowerToolStripMenuItem_Click);
            // 
            // cubeRotationToolStripMenuItem
            // 
            this.cubeRotationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.rotate90CWToolStripMenuItem,
            this.rotate90CCWToolStripMenuItem,
            this.rotate180ToolStripMenuItem});
            this.cubeRotationToolStripMenuItem.Name = "cubeRotationToolStripMenuItem";
            this.cubeRotationToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.cubeRotationToolStripMenuItem.Text = "Cube Rotation";
            // 
            // rotate90CWToolStripMenuItem
            // 
            this.rotate90CWToolStripMenuItem.Name = "rotate90CWToolStripMenuItem";
            this.rotate90CWToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.rotate90CWToolStripMenuItem.Text = "Rotate 90 CW";
            this.rotate90CWToolStripMenuItem.Click += new System.EventHandler(this.rotate90CWToolStripMenuItem_Click);
            // 
            // rotate90CCWToolStripMenuItem
            // 
            this.rotate90CCWToolStripMenuItem.Name = "rotate90CCWToolStripMenuItem";
            this.rotate90CCWToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.rotate90CCWToolStripMenuItem.Text = "Rotate 90 CCW";
            this.rotate90CCWToolStripMenuItem.Click += new System.EventHandler(this.rotate90CCWToolStripMenuItem_Click);
            // 
            // rotate180ToolStripMenuItem
            // 
            this.rotate180ToolStripMenuItem.Name = "rotate180ToolStripMenuItem";
            this.rotate180ToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.rotate180ToolStripMenuItem.Text = "Rotate 180";
            this.rotate180ToolStripMenuItem.Click += new System.EventHandler(this.rotate180ToolStripMenuItem_Click);
            // 
            // platformsToolStripMenuItem
            // 
            this.platformsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pushIntoToolStripMenuItem,
            this.pullAwayToolStripMenuItem,
            this.rotateTo90ToolStripMenuItem,
            this.rotateTo0ToolStripMenuItem});
            this.platformsToolStripMenuItem.Name = "platformsToolStripMenuItem";
            this.platformsToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.platformsToolStripMenuItem.Text = "Platforms";
            // 
            // pushIntoToolStripMenuItem
            // 
            this.pushIntoToolStripMenuItem.Name = "pushIntoToolStripMenuItem";
            this.pushIntoToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.pushIntoToolStripMenuItem.Text = "Push Into";
            this.pushIntoToolStripMenuItem.Click += new System.EventHandler(this.pushIntoToolStripMenuItem_Click);
            // 
            // pullAwayToolStripMenuItem
            // 
            this.pullAwayToolStripMenuItem.Name = "pullAwayToolStripMenuItem";
            this.pullAwayToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.pullAwayToolStripMenuItem.Text = "Pull Away";
            this.pullAwayToolStripMenuItem.Click += new System.EventHandler(this.pullAwayToolStripMenuItem_Click);
            // 
            // rotateTo90ToolStripMenuItem
            // 
            this.rotateTo90ToolStripMenuItem.Name = "rotateTo90ToolStripMenuItem";
            this.rotateTo90ToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.rotateTo90ToolStripMenuItem.Text = "Rotate to 90";
            this.rotateTo90ToolStripMenuItem.Click += new System.EventHandler(this.rotateTo90ToolStripMenuItem_Click);
            // 
            // rotateTo0ToolStripMenuItem
            // 
            this.rotateTo0ToolStripMenuItem.Name = "rotateTo0ToolStripMenuItem";
            this.rotateTo0ToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.rotateTo0ToolStripMenuItem.Text = "Rotate to 0";
            this.rotateTo0ToolStripMenuItem.Click += new System.EventHandler(this.rotateTo0ToolStripMenuItem_Click);
            // 
            // lEDToolStripMenuItem
            // 
            this.lEDToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.turnOnToolStripMenuItem,
            this.turnOffToolStripMenuItem});
            this.lEDToolStripMenuItem.Name = "lEDToolStripMenuItem";
            this.lEDToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.lEDToolStripMenuItem.Text = "LED";
            // 
            // turnOnToolStripMenuItem
            // 
            this.turnOnToolStripMenuItem.Name = "turnOnToolStripMenuItem";
            this.turnOnToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.turnOnToolStripMenuItem.Text = "Turn on";
            this.turnOnToolStripMenuItem.Click += new System.EventHandler(this.turnOnToolStripMenuItem_Click);
            // 
            // turnOffToolStripMenuItem
            // 
            this.turnOffToolStripMenuItem.Name = "turnOffToolStripMenuItem";
            this.turnOffToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.turnOffToolStripMenuItem.Text = "Turn off";
            this.turnOffToolStripMenuItem.Click += new System.EventHandler(this.turnOffToolStripMenuItem_Click);
            // 
            // cameraToolStripMenuItem
            // 
            this.cameraToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.devicesToolStripMenuItem,
            this.captureSideToolStripMenuItem});
            this.cameraToolStripMenuItem.Name = "cameraToolStripMenuItem";
            this.cameraToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.cameraToolStripMenuItem.Text = "Camera";
            this.cameraToolStripMenuItem.DropDownClosed += new System.EventHandler(this.cameraToolStripMenuItem_DropDownClosed);
            this.cameraToolStripMenuItem.DropDownOpened += new System.EventHandler(this.cameraToolStripMenuItem_DropDownOpened);
            // 
            // devicesToolStripMenuItem
            // 
            this.devicesToolStripMenuItem.Name = "devicesToolStripMenuItem";
            this.devicesToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.devicesToolStripMenuItem.Text = "Input Devices";
            // 
            // captureSideToolStripMenuItem
            // 
            this.captureSideToolStripMenuItem.Name = "captureSideToolStripMenuItem";
            this.captureSideToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.captureSideToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.captureSideToolStripMenuItem.Text = "Capture Side";
            this.captureSideToolStripMenuItem.Click += new System.EventHandler(this.captureSideToolStripMenuItem_Click);
            // 
            // quitToolStripMenuItem
            // 
            this.quitToolStripMenuItem.Name = "quitToolStripMenuItem";
            this.quitToolStripMenuItem.Size = new System.Drawing.Size(42, 20);
            this.quitToolStripMenuItem.Text = "Quit";
            this.quitToolStripMenuItem.Click += new System.EventHandler(this.quitToolStripMenuItem_Click);
            // 
            // tmrRefreshCaptureDevices
            // 
            this.tmrRefreshCaptureDevices.Enabled = true;
            this.tmrRefreshCaptureDevices.Interval = 3000;
            this.tmrRefreshCaptureDevices.Tick += new System.EventHandler(this.tmrRefreshCaptureDevices_Tick);
            // 
            // cfcBottom
            // 
            this.cfcBottom.Face00 = CubeFaceControl.CubeColors.WHITE;
            this.cfcBottom.Face01 = CubeFaceControl.CubeColors.WHITE;
            this.cfcBottom.Face02 = CubeFaceControl.CubeColors.WHITE;
            this.cfcBottom.Face10 = CubeFaceControl.CubeColors.WHITE;
            this.cfcBottom.Face11 = CubeFaceControl.CubeColors.WHITE;
            this.cfcBottom.Face12 = CubeFaceControl.CubeColors.WHITE;
            this.cfcBottom.Face20 = CubeFaceControl.CubeColors.WHITE;
            this.cfcBottom.Face21 = CubeFaceControl.CubeColors.WHITE;
            this.cfcBottom.Face22 = CubeFaceControl.CubeColors.WHITE;
            this.cfcBottom.Location = new System.Drawing.Point(667, 677);
            this.cfcBottom.Name = "cfcBottom";
            this.cfcBottom.Size = new System.Drawing.Size(200, 200);
            this.cfcBottom.TabIndex = 12;
            // 
            // cfcLeft
            // 
            this.cfcLeft.Face00 = CubeFaceControl.CubeColors.WHITE;
            this.cfcLeft.Face01 = CubeFaceControl.CubeColors.WHITE;
            this.cfcLeft.Face02 = CubeFaceControl.CubeColors.WHITE;
            this.cfcLeft.Face10 = CubeFaceControl.CubeColors.WHITE;
            this.cfcLeft.Face11 = CubeFaceControl.CubeColors.WHITE;
            this.cfcLeft.Face12 = CubeFaceControl.CubeColors.WHITE;
            this.cfcLeft.Face20 = CubeFaceControl.CubeColors.WHITE;
            this.cfcLeft.Face21 = CubeFaceControl.CubeColors.WHITE;
            this.cfcLeft.Face22 = CubeFaceControl.CubeColors.WHITE;
            this.cfcLeft.Location = new System.Drawing.Point(461, 471);
            this.cfcLeft.Name = "cfcLeft";
            this.cfcLeft.Size = new System.Drawing.Size(200, 200);
            this.cfcLeft.TabIndex = 12;
            // 
            // cfcBack
            // 
            this.cfcBack.Face00 = CubeFaceControl.CubeColors.WHITE;
            this.cfcBack.Face01 = CubeFaceControl.CubeColors.WHITE;
            this.cfcBack.Face02 = CubeFaceControl.CubeColors.WHITE;
            this.cfcBack.Face10 = CubeFaceControl.CubeColors.WHITE;
            this.cfcBack.Face11 = CubeFaceControl.CubeColors.WHITE;
            this.cfcBack.Face12 = CubeFaceControl.CubeColors.WHITE;
            this.cfcBack.Face20 = CubeFaceControl.CubeColors.WHITE;
            this.cfcBack.Face21 = CubeFaceControl.CubeColors.WHITE;
            this.cfcBack.Face22 = CubeFaceControl.CubeColors.WHITE;
            this.cfcBack.Location = new System.Drawing.Point(1079, 471);
            this.cfcBack.Name = "cfcBack";
            this.cfcBack.Size = new System.Drawing.Size(200, 200);
            this.cfcBack.TabIndex = 12;
            // 
            // cfcRight
            // 
            this.cfcRight.Face00 = CubeFaceControl.CubeColors.WHITE;
            this.cfcRight.Face01 = CubeFaceControl.CubeColors.WHITE;
            this.cfcRight.Face02 = CubeFaceControl.CubeColors.WHITE;
            this.cfcRight.Face10 = CubeFaceControl.CubeColors.WHITE;
            this.cfcRight.Face11 = CubeFaceControl.CubeColors.WHITE;
            this.cfcRight.Face12 = CubeFaceControl.CubeColors.WHITE;
            this.cfcRight.Face20 = CubeFaceControl.CubeColors.WHITE;
            this.cfcRight.Face21 = CubeFaceControl.CubeColors.WHITE;
            this.cfcRight.Face22 = CubeFaceControl.CubeColors.WHITE;
            this.cfcRight.Location = new System.Drawing.Point(873, 471);
            this.cfcRight.Name = "cfcRight";
            this.cfcRight.Size = new System.Drawing.Size(200, 200);
            this.cfcRight.TabIndex = 12;
            // 
            // cfcTop
            // 
            this.cfcTop.Face00 = CubeFaceControl.CubeColors.WHITE;
            this.cfcTop.Face01 = CubeFaceControl.CubeColors.WHITE;
            this.cfcTop.Face02 = CubeFaceControl.CubeColors.WHITE;
            this.cfcTop.Face10 = CubeFaceControl.CubeColors.WHITE;
            this.cfcTop.Face11 = CubeFaceControl.CubeColors.WHITE;
            this.cfcTop.Face12 = CubeFaceControl.CubeColors.WHITE;
            this.cfcTop.Face20 = CubeFaceControl.CubeColors.WHITE;
            this.cfcTop.Face21 = CubeFaceControl.CubeColors.WHITE;
            this.cfcTop.Face22 = CubeFaceControl.CubeColors.WHITE;
            this.cfcTop.Location = new System.Drawing.Point(667, 265);
            this.cfcTop.Name = "cfcTop";
            this.cfcTop.Size = new System.Drawing.Size(200, 200);
            this.cfcTop.TabIndex = 12;
            // 
            // cfcFront
            // 
            this.cfcFront.Face00 = CubeFaceControl.CubeColors.WHITE;
            this.cfcFront.Face01 = CubeFaceControl.CubeColors.WHITE;
            this.cfcFront.Face02 = CubeFaceControl.CubeColors.WHITE;
            this.cfcFront.Face10 = CubeFaceControl.CubeColors.WHITE;
            this.cfcFront.Face11 = CubeFaceControl.CubeColors.WHITE;
            this.cfcFront.Face12 = CubeFaceControl.CubeColors.WHITE;
            this.cfcFront.Face20 = CubeFaceControl.CubeColors.WHITE;
            this.cfcFront.Face21 = CubeFaceControl.CubeColors.WHITE;
            this.cfcFront.Face22 = CubeFaceControl.CubeColors.WHITE;
            this.cfcFront.Location = new System.Drawing.Point(667, 471);
            this.cfcFront.Name = "cfcFront";
            this.cfcFront.Size = new System.Drawing.Size(200, 200);
            this.cfcFront.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 15F);
            this.label2.Location = new System.Drawing.Point(3, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(169, 24);
            this.label2.TabIndex = 20;
            this.label2.Text = "Time To Solution:";
            // 
            // lblRemainingTime
            // 
            this.lblRemainingTime.Font = new System.Drawing.Font("Digital-7", 50F);
            this.lblRemainingTime.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblRemainingTime.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblRemainingTime.Location = new System.Drawing.Point(171, 0);
            this.lblRemainingTime.Name = "lblRemainingTime";
            this.lblRemainingTime.Size = new System.Drawing.Size(173, 76);
            this.lblRemainingTime.TabIndex = 21;
            this.lblRemainingTime.Text = "--:--";
            this.lblRemainingTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.lblRemainingTime);
            this.panel2.Location = new System.Drawing.Point(1239, 51);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(344, 76);
            this.panel2.TabIndex = 22;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.lblRemainingMoves);
            this.panel3.Location = new System.Drawing.Point(1239, 133);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(344, 79);
            this.panel3.TabIndex = 23;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 15F);
            this.label1.Location = new System.Drawing.Point(3, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(173, 24);
            this.label1.TabIndex = 20;
            this.label1.Text = "Remaining Moves:";
            // 
            // lblRemainingMoves
            // 
            this.lblRemainingMoves.Font = new System.Drawing.Font("Digital-7", 50F);
            this.lblRemainingMoves.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblRemainingMoves.Location = new System.Drawing.Point(172, 0);
            this.lblRemainingMoves.Name = "lblRemainingMoves";
            this.lblRemainingMoves.Size = new System.Drawing.Size(172, 79);
            this.lblRemainingMoves.TabIndex = 21;
            this.lblRemainingMoves.Text = "-";
            this.lblRemainingMoves.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.button2.Location = new System.Drawing.Point(12, 523);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(424, 38);
            this.button2.TabIndex = 25;
            this.button2.Text = "Confirm Changes";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label3);
            this.panel4.Controls.Add(this.lblCurrentMove);
            this.panel4.Location = new System.Drawing.Point(1239, 218);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(344, 82);
            this.panel4.TabIndex = 23;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 15F);
            this.label3.Location = new System.Drawing.Point(3, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(136, 24);
            this.label3.TabIndex = 20;
            this.label3.Text = "Current Move:";
            // 
            // lblCurrentMove
            // 
            this.lblCurrentMove.Font = new System.Drawing.Font("Tahoma", 35F);
            this.lblCurrentMove.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblCurrentMove.Location = new System.Drawing.Point(170, 0);
            this.lblCurrentMove.Name = "lblCurrentMove";
            this.lblCurrentMove.Size = new System.Drawing.Size(174, 82);
            this.lblCurrentMove.TabIndex = 21;
            this.lblCurrentMove.Text = "-";
            this.lblCurrentMove.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tmrTimeToSolution
            // 
            this.tmrTimeToSolution.Interval = 1000;
            this.tmrTimeToSolution.Tick += new System.EventHandler(this.tmrTimeToSolution_Tick);
            // 
            // Form1
            // 
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1612, 912);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.btnSolve);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.cfcBottom);
            this.Controls.Add(this.cfcLeft);
            this.Controls.Add(this.cfcBack);
            this.Controls.Add(this.cfcRight);
            this.Controls.Add(this.cfcTop);
            this.Controls.Add(this.cfcFront);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.imgVideoFeed)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imgMarker9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMarker5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMarker8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMarker1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMarker2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMarker7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMarker3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMarker4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMarker6)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Emgu.CV.UI.ImageBox imgVideoFeed;
        private CubeFaceControl.UserControl1 cfcFront;
        private CubeFaceControl.UserControl1 cfcTop;
        private CubeFaceControl.UserControl1 cfcRight;
        private CubeFaceControl.UserControl1 cfcLeft;
        private CubeFaceControl.UserControl1 cfcBottom;
        private CubeFaceControl.UserControl1 cfcBack;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox imgMarker9;
        private System.Windows.Forms.PictureBox imgMarker5;
        private System.Windows.Forms.PictureBox imgMarker8;
        private System.Windows.Forms.PictureBox imgMarker1;
        private System.Windows.Forms.PictureBox imgMarker2;
        private System.Windows.Forms.PictureBox imgMarker7;
        private System.Windows.Forms.PictureBox imgMarker3;
        private System.Windows.Forms.PictureBox imgMarker4;
        private System.Windows.Forms.PictureBox imgMarker6;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Button btnSolve;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem calibrationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem systemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem actionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cubeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem elevateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lowerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cubeRotationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rotate90CWToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rotate90CCWToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rotate180ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem platformsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pushIntoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pullAwayToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rotateTo90ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rotateTo0ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cameraToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem captureSideToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem devicesToolStripMenuItem;
        private System.Windows.Forms.Timer tmrRefreshCaptureDevices;
        private System.Windows.Forms.ToolStripMenuItem lEDToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem turnOnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem turnOffToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem colorsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem calibrateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clearModelDataToolStripMenuItem;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblRemainingTime;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblRemainingMoves;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblCurrentMove;
        private System.Windows.Forms.Timer tmrTimeToSolution;
        private System.Windows.Forms.ToolStripMenuItem quitToolStripMenuItem;
    }
}

