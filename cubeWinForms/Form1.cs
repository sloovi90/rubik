﻿using System;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.UI;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;
using Emgu.CV.Util;
using Emgu.Util;
using System.Drawing;
using System.Diagnostics;
using System.Collections.Generic;
using System.ComponentModel;
using CubeFaceControl;
using System.IO.Ports;
using DirectShowLib;


namespace cubeWinForms
{
    public partial class Form1 : Form
    {
        const string BLUETOOTH_MAC = /*"48:59:29:95:a5:b7";*/"98:d3:31:40:63:1b";
        private int[] FACE_CAPTURE_ORDER = { (int)Side.FRONT, (int)Side.LEFT, (int)Side.BACK, (int)Side.DOWN, (int)Side.RIGHT, (int)Side.TOP };

        Capture capture;
        List<UserControl1> faceControls;
        List<PictureBox> markers;
        Cube rubik;
        RobotProxy proxy;
        ColorProvider colorProvider;
        WaitingDialog dialog;
        Bluetooth bt;
        int timeToSolution = 0;
        int camera = 0;

        public Form1()
        {
            InitializeComponent();

            // Initialize the rectangular markers on the video feed
            markers = new List<PictureBox>();
            markers.Add(imgMarker1);
            markers.Add(imgMarker2);
            markers.Add(imgMarker3);
            markers.Add(imgMarker4);
            markers.Add(imgMarker5);
            markers.Add(imgMarker6);
            markers.Add(imgMarker7);
            markers.Add(imgMarker8);
            markers.Add(imgMarker9);

            foreach(PictureBox p in markers)
                p.Parent = imgVideoFeed;

            // Initialize the Bluetooth class
            bt = new Bluetooth();
            bt.Connect(BLUETOOTH_MAC);

            // Initialize the face controls
            faceControls = new List<UserControl1>();
            faceControls.Add(cfcTop);
            faceControls.Add(cfcRight);
            faceControls.Add(cfcFront);
            faceControls.Add(cfcBottom);
            faceControls.Add(cfcLeft);
            faceControls.Add(cfcBack);

            dialog = new WaitingDialog("Initializing application, please wait...");
            dialog.Show();

            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += (sender, e) => { colorProvider = new ColorProvider(); };
            worker.RunWorkerCompleted += (sender, e) =>
            {
                dialog.Close();
                InitCapture();
            };
            worker.RunWorkerAsync();

            btnSolve.Focus();
        }

        private void Solve()
        {
            // Step 1: Use the robot and the HD camera to capture all the faces
            BackgroundWorker wb = new BackgroundWorker();
            wb.DoWork += CaptureCube;
            // Step 2: When done, send the result to the solving algorithm
            wb.RunWorkerCompleted += (sender, e) => { InitCubeFromUI(true); SendToAlgorithm(); };
            wb.RunWorkerAsync();
        }
        
        private void CaptureCube(object sender, DoWorkEventArgs e)
        {
            bt.SendCommandAndWait("r");
            CaptureFace(); //capture Front
            bt.SendCommandAndWait("a");
            CaptureFace(); ; //capture Left
            bt.SendCommandAndWait("a");
            CaptureFace(); //capture Back
            bt.SendCommandAndWait("UCPucUpu");
            CaptureFace(); //capture Down
            bt.SendCommandAndWait("a");
            CaptureFace(); //capture Right
            bt.SendCommandAndWait("a");
            CaptureFace(); //capture Up
            bt.SendCommandAndWait("UCPucUpu");
        }

        private void SendToAlgorithm()
        {
            // Get the cube structure string
            string str = rubik.GetCubeString();

            if (!ValidateCubeString(str))
            {
                MessageBox.Show("Cube was not captured correctly, please check again");
                button2.Invoke(new Action(() => { button2.Show(); })); // Show the "Confirm Changes" button
                return;
            }

            WaitingDialog solvingDialog = new WaitingDialog("Solving with the Kociemba algorithm...");

            BackgroundWorker b = new BackgroundWorker();
            b.DoWork += (sender, e) => { e.Result = Utils.GetAlgorithmSolution((string)e.Argument); };
            b.RunWorkerCompleted += (sender, e) => {
                Solution solution = new Solution((string)e.Result, rubik);
                lblRemainingMoves.Text = solution.Length.ToString();
                this.Refresh();
                solvingDialog.Hide();
                SolveByRobot(solution); // Step 3: Solve the cube! 
            };

            solvingDialog.Show();
            b.RunWorkerAsync(str);
        }

        delegate void NoArgsDelegate();
        delegate void MoveDelegate(Move move);
        delegate void IntDelegate(int remaining);
        private void SolveByRobot(Solution solution)
        {
            BackgroundWorker wb = new BackgroundWorker();
            wb.DoWork += (sender, e) =>
            {
                MoveDelegate m1 = (mm) => { lblCurrentMove.Text = mm.ToString(); };
                IntDelegate d1 = (dd) => { lblRemainingMoves.Text = dd.ToString(); };
                NoArgsDelegate n1 = () => { this.Refresh(); };

                foreach (Tuple<Move, string, int> m in solution.MoveGenerator())
                {
                    Move move = m.Item1;
                    timeToSolution = RobotProxy.GetRobotSolutionDuration(m.Item2);
                    PerformMove(move);

                    lblRemainingMoves.Invoke(d1, new object[] { m.Item3 });
                    lblCurrentMove.Invoke(m1, new object[] { move });
                    this.Invoke(n1);
                }
            };
            wb.RunWorkerAsync();
            tmrTimeToSolution.Start();
        }

        private int iterator = 0;
        delegate Image<Bgr, byte> GetFrameDelegate();
        delegate void FillFaceDelegate(UserControl1 face, CubeColors[] colors);
        private void CaptureFace()
        {
            
            GetFrameDelegate frameDelegate = GetFrameFromUI;
            FillFaceDelegate faceDelegate = FillFaceControl;

            //bt.SendCommandAndWait("L");
            System.Threading.Thread.Sleep(1600);
            Image<Bgr, byte> frame = (Image<Bgr, byte>)imgVideoFeed.Invoke(frameDelegate);
            //bt.SendCommandAndWait("l");

            CubeColors[] colors = IdentifyFaceColors(frame);
            faceControls[0].Invoke(faceDelegate, new object[] { faceControls[FACE_CAPTURE_ORDER[iterator]], colors });
            iterator++;

            if (iterator > 6)
                throw new InvalidOperationException("Finished capturing all faces");
        }

        private Image<Bgr, byte> GetFrameFromUI()
        {
            return (Image<Bgr, byte>)imgVideoFeed.Image;
        }

        private void InitCubeFromUI(bool fixSideRotations)
        {
            rubik = new Cube();
            proxy = new RobotProxy(rubik, Side.DOWN, RobotProxy.SideOrientation.STRAIGHT, bt);

            //int[] indexes = { (int)Side.FRONT, (int)Side.LEFT, (int)Side.BACK,(int)Side.BOTTOM, (int)Side.RIGHT, (int)Side.TOP };
            for (int j=0; j <= 5; ++j)
            {
                int[] arr = faceControls[FACE_CAPTURE_ORDER[j]].getFaceArray();
                              
                CubeColors[] cArr = new CubeColors[9];
                for (int i = 0; i < 9; ++i)
                    cArr[i] = (CubeColors)(arr[i]);
                Face f = new Face(cArr);
                rubik.SetFaceBySide((Side)FACE_CAPTURE_ORDER[j], f);
            }

            // Rotate 

            if (fixSideRotations)
            {
                rubik.GetFaceBySide(Side.RIGHT).RotateClockwise();
                rubik.GetFaceBySide(Side.DOWN).RotateClockwise();
                rubik.GetFaceBySide(Side.DOWN).RotateClockwise();
            }

            UpdateCubeControls();
        }

        private void InitCapture(int deviceIndex = 0)
        {
            capture = new Capture(deviceIndex);
            Application.Idle += ProcessFrame;
        }

        private void PopulateCaptureDevices()
        {
            DsDevice[] devices = DsDevice.GetDevicesOfCat(FilterCategory.VideoInputDevice);
            devicesToolStripMenuItem.DropDownItems.Clear();

            for(int i = 0; i < devices.Length; i++)
            {
                devicesToolStripMenuItem.DropDownItems.Add(devices[i].Name, null, 
                    (sender, e) => {
                        int cam = devicesToolStripMenuItem.DropDownItems.IndexOf((ToolStripItem)sender);
                        DestroyCapture();
                        InitCapture(cam);
                        camera = cam;
                        Console.WriteLine("Switching to camera " + cam);
                    });
            }
        }

        private void DestroyCapture()
        {
            try
            {
                Application.Idle -= ProcessFrame;
                capture.Dispose();
            } catch (Exception e) { }
        }

        private void ProcessFrame(object sender, EventArgs arg)
        {
            try
            {
                Image<Bgr, Byte> img = capture.QueryFrame().ToImage<Bgr, byte>();

                // Crop image from 16:9 to 1:1 from the center. NOTE: This assumes that the input image is 16:9
                img.ROI = new Rectangle(x: img.Width / 32 * 7, y: 0, width: img.Height, height: img.Height);
                // Resize to 400x400
                img = img.Resize(400, 400, Inter.Linear);

                imgVideoFeed.Image = img;
            } catch(Exception e) { }
        }

        private CubeColors[] IdentifyFaceColors(Image<Bgr, byte> frame)
        {
            CubeColors[] colors = new CubeColors[9];
            for (int i = 0; i < markers.Count; i++)
            {
                Image<Bgr, byte> subImage = frame.GetSubRect(new Rectangle(markers[i].Location, markers[i].Size));
                Bgr avgColor = subImage.GetAverage();
                colors[i] = isColor(avgColor);
            }

            return colors;
        }
        private void FillFaceStart(int iterator, UserControl1 face, CubeColors[] colors)
        {
            if (iterator != 3 && iterator != 4)
            {
                face.Face00 = (CubeFaceControl.CubeColors)colors[0];
                face.Face01 = (CubeFaceControl.CubeColors)colors[1];
                face.Face02 = (CubeFaceControl.CubeColors)colors[2];
                face.Face10 = (CubeFaceControl.CubeColors)colors[3];
                face.Face11 = (CubeFaceControl.CubeColors)colors[4];
                face.Face12 = (CubeFaceControl.CubeColors)colors[5];
                face.Face20 = (CubeFaceControl.CubeColors)colors[6];
                face.Face21 = (CubeFaceControl.CubeColors)colors[7];
                face.Face22 = (CubeFaceControl.CubeColors)colors[8];
            } else if (iterator == 3) {
                face.Face00 = (CubeFaceControl.CubeColors)colors[8];
                face.Face01 = (CubeFaceControl.CubeColors)colors[7];
                face.Face02 = (CubeFaceControl.CubeColors)colors[6];
                face.Face10 = (CubeFaceControl.CubeColors)colors[5];
                face.Face11 = (CubeFaceControl.CubeColors)colors[4];
                face.Face12 = (CubeFaceControl.CubeColors)colors[3];
                face.Face20 = (CubeFaceControl.CubeColors)colors[2];
                face.Face21 = (CubeFaceControl.CubeColors)colors[1];
                face.Face22 = (CubeFaceControl.CubeColors)colors[0];
            } else {
                face.Face00 = (CubeFaceControl.CubeColors)colors[6];
                face.Face01 = (CubeFaceControl.CubeColors)colors[3];
                face.Face02 = (CubeFaceControl.CubeColors)colors[0];
                face.Face10 = (CubeFaceControl.CubeColors)colors[7];
                face.Face11 = (CubeFaceControl.CubeColors)colors[4];
                face.Face12 = (CubeFaceControl.CubeColors)colors[1];
                face.Face20 = (CubeFaceControl.CubeColors)colors[8];
                face.Face21 = (CubeFaceControl.CubeColors)colors[5];
                face.Face22 = (CubeFaceControl.CubeColors)colors[2];
            }
        }
        private void FillFaceControl(UserControl1 face, CubeColors[] colors)
        {

            face.Face00 = (CubeFaceControl.CubeColors)colors[0];
            face.Face01 = (CubeFaceControl.CubeColors)colors[1];
            face.Face02 = (CubeFaceControl.CubeColors)colors[2];
            face.Face10 = (CubeFaceControl.CubeColors)colors[3];
            face.Face11 = (CubeFaceControl.CubeColors)colors[4];
            face.Face12 = (CubeFaceControl.CubeColors)colors[5];
            face.Face20 = (CubeFaceControl.CubeColors)colors[6];
            face.Face21 = (CubeFaceControl.CubeColors)colors[7];
            face.Face22 = (CubeFaceControl.CubeColors)colors[8];
            Refresh();
        }

        public CubeColors isColor(Bgr bgr)
        {
            return colorProvider.GetCubeColor(bgr);
        }

        private void UpdateCubeControls()
        {
            foreach (Side s in Enum.GetValues(typeof(Side)))
                FillFaceControl(faceControls[(int)s], rubik.GetFaceBySide(s).face);
            Refresh();
        }

        private void PerformMove(Move move)
        {
            //System.Threading.Thread.Sleep(800);
            proxy.MakeMove(move);
            //rubik.makePhysicalMoveViaBluetooth(bt, move);
            MoveDelegate md = (Move m) =>
            {
                rubik.MakeMove(move);
                UpdateCubeControls();
            };
            Invoke(md, new object[] { move });
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ResetState();
            Solve();
        }

        private void ResetState()
        {
            // Empty the cube controls
            foreach(UserControl1 c in faceControls)
                c.ResetSquares();

            rubik = new Cube();
            iterator = 0;
        }

        private bool ValidateCubeString(string str)
        {
            string[] strArr = { "F", "U", "D", "B", "R", "L" };
            foreach(string s in strArr)
            {
                if (str.Length - str.Replace(s, "").Length != 9)
                    return false;
            }
            return true;
        }

        private void systemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BackgroundWorker bw = new BackgroundWorker();
            bw.DoWork += (s, ee) => { bt.SendCommandAndWait("r"); };
            bw.RunWorkerCompleted += (s, ee) => { new SystemCalibration(bt).Show(); };
            bw.RunWorkerAsync();
        }

        private void elevateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bt.SendCommand("U");
        }

        private void lowerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bt.SendCommand("u");
        }

        private void rotate90CWToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bt.SendCommand("A");
        }

        private void rotate90CCWToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bt.SendCommand("a");
        }

        private void rotate180ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bt.SendCommand("Z");
        }

        private void pushIntoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bt.SendCommand("P");
        }

        private void pullAwayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bt.SendCommand("p");
        }

        private void rotateTo90ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bt.SendCommand("C");
        }

        private void rotateTo0ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bt.SendCommand("c");
        }

        private void captureSideToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //CaptureFace(sender, null);
            Image<Bgr, byte> frame = (Image<Bgr, byte>)imgVideoFeed.Image;
            CubeColors[] colors = IdentifyFaceColors(frame);
            FillFaceStart(iterator, faceControls[iterator], colors);
            iterator++;
            if (iterator == 6)
            {
                InitCubeFromUI(true);
            }
            else if (iterator > 6)
                return;
        }

        private void tmrRefreshCaptureDevices_Tick(object sender, EventArgs e)
        {
            PopulateCaptureDevices();
        }

        private void cameraToolStripMenuItem_DropDownOpened(object sender, EventArgs e)
        {
            tmrRefreshCaptureDevices.Stop();
        }

        private void cameraToolStripMenuItem_DropDownClosed(object sender, EventArgs e)
        {
            tmrRefreshCaptureDevices.Start();
        }

        private void turnOnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bt.SendCommand("L");
        }

        private void turnOffToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bt.SendCommand("l");
        }

        private void calibrateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DestroyCapture(); // NOTE: This requires us to restart the program after calibration
            new ColorCalibration(camera).ShowDialog();
            InitCapture(camera);
        }

        private void clearModelDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("Do you want to truncate the learning model data?", "Confirm", MessageBoxButtons.YesNo) 
                == DialogResult.Yes)
            {
                AzureDatabase.Truncate("dbo.ColorTrainingData");
            }
        }
        int i = 0;
        private void button1_Click(object sender, EventArgs e)
        {
            if(i++ == 0)
            {
                rubik = new Cube();
                //rubik.BuildCubeFromString("UUUUUURRRDRRDRRDRRFFFFFFFFFLLLDDDDDDLLULLULLUBBBBBBBBB");
                rubik.BuildCubeFromString("BDFUULUFLDDDRRBURLFRBBFURFFBLRDDBFLURFLLLULRDRFUDBUBBD");
                UpdateCubeControls();
            } else
            {
                string s = rubik.GetCubeString();
                rubik = new Cube();
                rubik.BuildCubeFromString(s);
                //rubik.BuildCubeFromString("BDFUULUFLDDDRRBURLFRBBFURFFBLRDDBFLURFLLLULRDRFUDBUBBD");
                UpdateCubeControls();
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            InitCubeFromUI(false);
            //UpdateCubeControls();
            SendToAlgorithm();
        }

        private void tmrTimeToSolution_Tick(object sender, EventArgs e)
        {
            timeToSolution--;
            lblRemainingTime.Text = TimeSpan.FromSeconds(timeToSolution).ToString(@"mm\:ss");

            if (timeToSolution == 0)
                tmrTimeToSolution.Stop();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void quitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
