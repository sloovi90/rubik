﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cubeWinForms
{
    public class RobotProxy
    {
        public enum SideOrientation { STRAIGHT = 0, CLOCKWISE, UPSIDE_DOWN, COUNTERCLOCKWISE };

        public Cube Cube { get; set; }
        public Side DownSide { get; set; }
        public SideOrientation Orientation { get; set; }
        public Bluetooth Bluetooth { get; set; }

        public RobotProxy(Cube cube, Side downSide, SideOrientation orientation, Bluetooth bluetooth)
        {
            Cube = cube;
            DownSide = downSide;
            Orientation = orientation;
            this.Bluetooth = bluetooth;
        }

        public string MakeMove(Move move)
        {
            string str = "";
            CubeTuple tuple = new CubeTuple(DownSide, Orientation, move.Side);
            ResultTuple t = sideMap[tuple];
            str = t.Function.Invoke(Bluetooth);

            DownSide = move.Side;
            Orientation = t.NewOrientation;

            if (Bluetooth == null)
            {
                if (move.Direction == MoveDirection.CLOCKWISE)
                {
                    //Orientation = (SideOrientation)(((int)Orientation + 1) % 4);
                    str += "PapU";
                }
                else if (move.Direction == MoveDirection.COUNTERCLOCKWISE)
                {
                    str += "PApU";
                    //Orientation = (SideOrientation)(((int)Orientation + 3) % 4);
                }
                else
                {
                    //Orientation = (SideOrientation)(((int)Orientation + 2) % 4);
                    str += "PZpU";
                }
            } else
            {
                if (move.Direction == MoveDirection.CLOCKWISE)
                {
                    Bluetooth.SendCommandAndWait("PapU");
                    //Orientation = (SideOrientation)(((int)Orientation + 3) % 4);
                }
                else if (move.Direction == MoveDirection.COUNTERCLOCKWISE)
                {
                    //Orientation = (SideOrientation)(((int)Orientation + 1) % 4);
                    Bluetooth.SendCommandAndWait("PApU");
                }
                else
                {
                    //Orientation = (SideOrientation)(((int)Orientation + 2) % 4);
                    Bluetooth.SendCommandAndWait("PZpU");
                }
            }
            return str;
        }

        static string FrontToBottom(Bluetooth Bluetooth)
        {
            string command = "UcPuCUpc";
            if (Bluetooth == null)
            {
                return command;
            }
            Bluetooth.SendCommandAndWait(command);
            
            return "";
        }

        static string BottomToBottom(Bluetooth Bluetooth)
        {
            string command = "U";
            if (Bluetooth == null)
            {
                return command;
            }
            Bluetooth.SendCommandAndWait(command);
            return "";
        }

        static string TopToBottom(Bluetooth Bluetooth)
        {
            string command = "UFU";
            if (Bluetooth == null)
            {
                return command;
            }
            Bluetooth.SendCommandAndWait(command);
            return "";
        }

        static string BackToBottom(Bluetooth Bluetooth)
        {
            string command = "ZcUPuCUpc";
            //string command = "CUPucU";
            if (Bluetooth == null)
            {
                return command;
            }
            Bluetooth.SendCommandAndWait(command);
            return "";
        }

        static string RightToBottom(Bluetooth Bluetooth)
        {
            string command = "AUPuCUpc";
            if (Bluetooth == null)
            {
                return command;
            }
            Bluetooth.SendCommandAndWait(command);
            return "";
        }

        static string LeftToBottom(Bluetooth Bluetooth)
        {
            string command = "aUPuCUpc";
            if (Bluetooth == null)
            {
                return command;
            }
            Bluetooth.SendCommandAndWait(command);
            return "";
        }

        public class CubeTuple
        {
            Side FromSide { set; get; }
            SideOrientation FromSideOrientation { set; get; }
            Side ToSide { set; get; }

            public CubeTuple(Side FromSide, SideOrientation Orientation, Side ToSide)
            {
                this.FromSide = FromSide;
                this.FromSideOrientation = Orientation;
                this.ToSide = ToSide;
            }

            public override int GetHashCode()
            {
                return FromSide.GetHashCode() * 1000 + FromSideOrientation.GetHashCode() * 100 + ToSide.GetHashCode() * 10;
            }

            public override bool Equals(object obj)
            {
                if (!(obj is CubeTuple)) return false;

                CubeTuple c = (CubeTuple)obj;
                return FromSide == c.FromSide && FromSideOrientation == c.FromSideOrientation && ToSide == c.ToSide;
            }
        }

        public class ResultTuple
        {
            public Func<Bluetooth, string> Function { get; set; }
            public SideOrientation NewOrientation { get; set; }

            public ResultTuple(Func<Bluetooth, string> Function, SideOrientation Orientation)
            {
                this.Function = Function;
                this.NewOrientation = Orientation;
            }
        }


        public Dictionary<CubeTuple, ResultTuple> sideMap = new Dictionary<CubeTuple, ResultTuple>()
        {
            {new CubeTuple(Side.FRONT, SideOrientation.STRAIGHT, Side.FRONT), new ResultTuple(BottomToBottom, SideOrientation.STRAIGHT)},
            {new CubeTuple(Side.FRONT, SideOrientation.STRAIGHT, Side.RIGHT), new ResultTuple(RightToBottom, SideOrientation.COUNTERCLOCKWISE)},
            {new CubeTuple(Side.FRONT, SideOrientation.STRAIGHT, Side.LEFT), new ResultTuple(LeftToBottom, SideOrientation.CLOCKWISE)},
            {new CubeTuple(Side.FRONT, SideOrientation.STRAIGHT, Side.TOP), new ResultTuple(FrontToBottom, SideOrientation.STRAIGHT)},
            {new CubeTuple(Side.FRONT, SideOrientation.STRAIGHT, Side.DOWN), new ResultTuple(BackToBottom, SideOrientation.STRAIGHT)},
            {new CubeTuple(Side.FRONT, SideOrientation.STRAIGHT, Side.BACK), new ResultTuple(TopToBottom, SideOrientation.UPSIDE_DOWN)},

            {new CubeTuple(Side.FRONT, SideOrientation.CLOCKWISE, Side.FRONT), new ResultTuple(BottomToBottom, SideOrientation.CLOCKWISE)},
            {new CubeTuple(Side.FRONT, SideOrientation.CLOCKWISE, Side.RIGHT), new ResultTuple(BackToBottom, SideOrientation.COUNTERCLOCKWISE)},
            {new CubeTuple(Side.FRONT, SideOrientation.CLOCKWISE, Side.LEFT), new ResultTuple(FrontToBottom, SideOrientation.CLOCKWISE)},
            {new CubeTuple(Side.FRONT, SideOrientation.CLOCKWISE, Side.TOP), new ResultTuple(RightToBottom, SideOrientation.STRAIGHT)},
            {new CubeTuple(Side.FRONT, SideOrientation.CLOCKWISE, Side.DOWN), new ResultTuple(LeftToBottom, SideOrientation.UPSIDE_DOWN)},
            {new CubeTuple(Side.FRONT, SideOrientation.CLOCKWISE, Side.BACK), new ResultTuple(TopToBottom, SideOrientation.CLOCKWISE)},

            {new CubeTuple(Side.FRONT, SideOrientation.UPSIDE_DOWN, Side.FRONT), new ResultTuple(BottomToBottom, SideOrientation.UPSIDE_DOWN)},
            {new CubeTuple(Side.FRONT, SideOrientation.UPSIDE_DOWN, Side.RIGHT), new ResultTuple(LeftToBottom, SideOrientation.COUNTERCLOCKWISE)},
            {new CubeTuple(Side.FRONT, SideOrientation.UPSIDE_DOWN, Side.LEFT), new ResultTuple(RightToBottom, SideOrientation.CLOCKWISE)},
            {new CubeTuple(Side.FRONT, SideOrientation.UPSIDE_DOWN, Side.TOP), new ResultTuple(BackToBottom, SideOrientation.STRAIGHT)},
            {new CubeTuple(Side.FRONT, SideOrientation.UPSIDE_DOWN, Side.DOWN), new ResultTuple(FrontToBottom, SideOrientation.UPSIDE_DOWN)},
            {new CubeTuple(Side.FRONT, SideOrientation.UPSIDE_DOWN, Side.BACK), new ResultTuple(TopToBottom, SideOrientation.STRAIGHT)},

            {new CubeTuple(Side.FRONT, SideOrientation.COUNTERCLOCKWISE, Side.FRONT), new ResultTuple(BottomToBottom, SideOrientation.COUNTERCLOCKWISE)},
            {new CubeTuple(Side.FRONT, SideOrientation.COUNTERCLOCKWISE, Side.RIGHT), new ResultTuple(FrontToBottom, SideOrientation.COUNTERCLOCKWISE)},
            {new CubeTuple(Side.FRONT, SideOrientation.COUNTERCLOCKWISE, Side.LEFT), new ResultTuple(BackToBottom, SideOrientation.CLOCKWISE)},
            {new CubeTuple(Side.FRONT, SideOrientation.COUNTERCLOCKWISE, Side.TOP), new ResultTuple(LeftToBottom, SideOrientation.STRAIGHT)},
            {new CubeTuple(Side.FRONT, SideOrientation.COUNTERCLOCKWISE, Side.DOWN), new ResultTuple(RightToBottom, SideOrientation.UPSIDE_DOWN)},
            {new CubeTuple(Side.FRONT, SideOrientation.COUNTERCLOCKWISE, Side.BACK), new ResultTuple(TopToBottom, SideOrientation.COUNTERCLOCKWISE)},

            //RIGHT

            {new CubeTuple(Side.RIGHT, SideOrientation.STRAIGHT, Side.FRONT), new ResultTuple(LeftToBottom, SideOrientation.CLOCKWISE)},
            {new CubeTuple(Side.RIGHT, SideOrientation.STRAIGHT, Side.RIGHT), new ResultTuple(BottomToBottom, SideOrientation.STRAIGHT)},
            {new CubeTuple(Side.RIGHT, SideOrientation.STRAIGHT, Side.LEFT), new ResultTuple(TopToBottom, SideOrientation.UPSIDE_DOWN)},
            {new CubeTuple(Side.RIGHT, SideOrientation.STRAIGHT, Side.TOP), new ResultTuple(FrontToBottom, SideOrientation.CLOCKWISE)},
            {new CubeTuple(Side.RIGHT, SideOrientation.STRAIGHT, Side.DOWN), new ResultTuple(BackToBottom, SideOrientation.CLOCKWISE)},
            {new CubeTuple(Side.RIGHT, SideOrientation.STRAIGHT, Side.BACK), new ResultTuple(RightToBottom, SideOrientation.COUNTERCLOCKWISE)},

            {new CubeTuple(Side.RIGHT, SideOrientation.CLOCKWISE, Side.FRONT), new ResultTuple(FrontToBottom, SideOrientation.CLOCKWISE)},
            {new CubeTuple(Side.RIGHT, SideOrientation.CLOCKWISE, Side.RIGHT), new ResultTuple(BottomToBottom, SideOrientation.CLOCKWISE)},
            {new CubeTuple(Side.RIGHT, SideOrientation.CLOCKWISE, Side.LEFT), new ResultTuple(TopToBottom, SideOrientation.CLOCKWISE)},
            {new CubeTuple(Side.RIGHT, SideOrientation.CLOCKWISE, Side.TOP), new ResultTuple(RightToBottom, SideOrientation.CLOCKWISE)},
            {new CubeTuple(Side.RIGHT, SideOrientation.CLOCKWISE, Side.DOWN), new ResultTuple(LeftToBottom, SideOrientation.CLOCKWISE)},
            {new CubeTuple(Side.RIGHT, SideOrientation.CLOCKWISE, Side.BACK), new ResultTuple(BackToBottom, SideOrientation.COUNTERCLOCKWISE)},

            {new CubeTuple(Side.RIGHT, SideOrientation.UPSIDE_DOWN, Side.FRONT), new ResultTuple(RightToBottom, SideOrientation.CLOCKWISE)},
            {new CubeTuple(Side.RIGHT, SideOrientation.UPSIDE_DOWN, Side.RIGHT), new ResultTuple(BottomToBottom, SideOrientation.UPSIDE_DOWN)},
            {new CubeTuple(Side.RIGHT, SideOrientation.UPSIDE_DOWN, Side.LEFT), new ResultTuple(TopToBottom, SideOrientation.STRAIGHT)},
            {new CubeTuple(Side.RIGHT, SideOrientation.UPSIDE_DOWN, Side.TOP), new ResultTuple(BackToBottom, SideOrientation.CLOCKWISE)},
            {new CubeTuple(Side.RIGHT, SideOrientation.UPSIDE_DOWN, Side.DOWN), new ResultTuple(FrontToBottom, SideOrientation.CLOCKWISE)},
            {new CubeTuple(Side.RIGHT, SideOrientation.UPSIDE_DOWN, Side.BACK), new ResultTuple(LeftToBottom, SideOrientation.COUNTERCLOCKWISE)},

            {new CubeTuple(Side.RIGHT, SideOrientation.COUNTERCLOCKWISE, Side.FRONT), new ResultTuple(BackToBottom, SideOrientation.CLOCKWISE)},
            {new CubeTuple(Side.RIGHT, SideOrientation.COUNTERCLOCKWISE, Side.RIGHT), new ResultTuple(BottomToBottom, SideOrientation.COUNTERCLOCKWISE)},
            {new CubeTuple(Side.RIGHT, SideOrientation.COUNTERCLOCKWISE, Side.LEFT), new ResultTuple(TopToBottom, SideOrientation.COUNTERCLOCKWISE)},
            {new CubeTuple(Side.RIGHT, SideOrientation.COUNTERCLOCKWISE, Side.TOP), new ResultTuple(LeftToBottom, SideOrientation.CLOCKWISE)},
            {new CubeTuple(Side.RIGHT, SideOrientation.COUNTERCLOCKWISE, Side.DOWN), new ResultTuple(RightToBottom, SideOrientation.CLOCKWISE)},
            {new CubeTuple(Side.RIGHT, SideOrientation.COUNTERCLOCKWISE, Side.BACK), new ResultTuple(FrontToBottom, SideOrientation.COUNTERCLOCKWISE)},

            //LEFT
            
            {new CubeTuple(Side.LEFT, SideOrientation.STRAIGHT, Side.FRONT), new ResultTuple(RightToBottom, SideOrientation.COUNTERCLOCKWISE)},
            {new CubeTuple(Side.LEFT, SideOrientation.STRAIGHT, Side.RIGHT), new ResultTuple(TopToBottom, SideOrientation.UPSIDE_DOWN)},
            {new CubeTuple(Side.LEFT, SideOrientation.STRAIGHT, Side.LEFT), new ResultTuple(BottomToBottom, SideOrientation.STRAIGHT)},
            {new CubeTuple(Side.LEFT, SideOrientation.STRAIGHT, Side.TOP), new ResultTuple(FrontToBottom, SideOrientation.COUNTERCLOCKWISE)},
            {new CubeTuple(Side.LEFT, SideOrientation.STRAIGHT, Side.DOWN), new ResultTuple(BackToBottom, SideOrientation.COUNTERCLOCKWISE)},
            {new CubeTuple(Side.LEFT, SideOrientation.STRAIGHT, Side.BACK), new ResultTuple(LeftToBottom, SideOrientation.CLOCKWISE)},

            {new CubeTuple(Side.LEFT, SideOrientation.CLOCKWISE, Side.FRONT), new ResultTuple(BackToBottom, SideOrientation.COUNTERCLOCKWISE)},
            {new CubeTuple(Side.LEFT, SideOrientation.CLOCKWISE, Side.RIGHT), new ResultTuple(TopToBottom, SideOrientation.CLOCKWISE)},
            {new CubeTuple(Side.LEFT, SideOrientation.CLOCKWISE, Side.LEFT), new ResultTuple(BottomToBottom, SideOrientation.CLOCKWISE)},
            {new CubeTuple(Side.LEFT, SideOrientation.CLOCKWISE, Side.TOP), new ResultTuple(RightToBottom, SideOrientation.COUNTERCLOCKWISE)},
            {new CubeTuple(Side.LEFT, SideOrientation.CLOCKWISE, Side.DOWN), new ResultTuple(LeftToBottom, SideOrientation.COUNTERCLOCKWISE)},
            {new CubeTuple(Side.LEFT, SideOrientation.CLOCKWISE, Side.BACK), new ResultTuple(FrontToBottom, SideOrientation.CLOCKWISE)},

            {new CubeTuple(Side.LEFT, SideOrientation.UPSIDE_DOWN, Side.FRONT), new ResultTuple(LeftToBottom, SideOrientation.COUNTERCLOCKWISE)},
            {new CubeTuple(Side.LEFT, SideOrientation.UPSIDE_DOWN, Side.RIGHT), new ResultTuple(TopToBottom, SideOrientation.STRAIGHT)},
            {new CubeTuple(Side.LEFT, SideOrientation.UPSIDE_DOWN, Side.LEFT), new ResultTuple(BottomToBottom, SideOrientation.UPSIDE_DOWN)},
            {new CubeTuple(Side.LEFT, SideOrientation.UPSIDE_DOWN, Side.TOP), new ResultTuple(BackToBottom, SideOrientation.COUNTERCLOCKWISE)},
            {new CubeTuple(Side.LEFT, SideOrientation.UPSIDE_DOWN, Side.DOWN), new ResultTuple(FrontToBottom, SideOrientation.COUNTERCLOCKWISE)},
            {new CubeTuple(Side.LEFT, SideOrientation.UPSIDE_DOWN, Side.BACK), new ResultTuple(RightToBottom, SideOrientation.CLOCKWISE)},

            {new CubeTuple(Side.LEFT, SideOrientation.COUNTERCLOCKWISE, Side.FRONT), new ResultTuple(FrontToBottom, SideOrientation.COUNTERCLOCKWISE)},
            {new CubeTuple(Side.LEFT, SideOrientation.COUNTERCLOCKWISE, Side.RIGHT), new ResultTuple(TopToBottom, SideOrientation.COUNTERCLOCKWISE)},
            {new CubeTuple(Side.LEFT, SideOrientation.COUNTERCLOCKWISE, Side.LEFT), new ResultTuple(BottomToBottom, SideOrientation.COUNTERCLOCKWISE)},
            {new CubeTuple(Side.LEFT, SideOrientation.COUNTERCLOCKWISE, Side.TOP), new ResultTuple(LeftToBottom, SideOrientation.COUNTERCLOCKWISE)},
            {new CubeTuple(Side.LEFT, SideOrientation.COUNTERCLOCKWISE, Side.DOWN), new ResultTuple(RightToBottom, SideOrientation.UPSIDE_DOWN)},
            {new CubeTuple(Side.LEFT, SideOrientation.COUNTERCLOCKWISE, Side.BACK), new ResultTuple(BackToBottom, SideOrientation.CLOCKWISE)},

            //TOP
            
            {new CubeTuple(Side.TOP, SideOrientation.STRAIGHT, Side.FRONT), new ResultTuple(BackToBottom, SideOrientation.UPSIDE_DOWN)},
            {new CubeTuple(Side.TOP, SideOrientation.STRAIGHT, Side.RIGHT), new ResultTuple(RightToBottom, SideOrientation.UPSIDE_DOWN)},
            {new CubeTuple(Side.TOP, SideOrientation.STRAIGHT, Side.LEFT), new ResultTuple(LeftToBottom, SideOrientation.UPSIDE_DOWN)},
            {new CubeTuple(Side.TOP, SideOrientation.STRAIGHT, Side.TOP), new ResultTuple(BottomToBottom, SideOrientation.STRAIGHT)},
            {new CubeTuple(Side.TOP, SideOrientation.STRAIGHT, Side.DOWN), new ResultTuple(TopToBottom, SideOrientation.STRAIGHT)},
            {new CubeTuple(Side.TOP, SideOrientation.STRAIGHT, Side.BACK), new ResultTuple(FrontToBottom, SideOrientation.UPSIDE_DOWN)},

            {new CubeTuple(Side.TOP, SideOrientation.CLOCKWISE, Side.FRONT), new ResultTuple(LeftToBottom, SideOrientation.UPSIDE_DOWN)},
            {new CubeTuple(Side.TOP, SideOrientation.CLOCKWISE, Side.RIGHT), new ResultTuple(BackToBottom, SideOrientation.UPSIDE_DOWN)},
            {new CubeTuple(Side.TOP, SideOrientation.CLOCKWISE, Side.LEFT), new ResultTuple(FrontToBottom, SideOrientation.UPSIDE_DOWN)},
            {new CubeTuple(Side.TOP, SideOrientation.CLOCKWISE, Side.TOP), new ResultTuple(BottomToBottom, SideOrientation.CLOCKWISE)},
            {new CubeTuple(Side.TOP, SideOrientation.CLOCKWISE, Side.DOWN), new ResultTuple(TopToBottom, SideOrientation.COUNTERCLOCKWISE)},
            {new CubeTuple(Side.TOP, SideOrientation.CLOCKWISE, Side.BACK), new ResultTuple(RightToBottom, SideOrientation.UPSIDE_DOWN)},

            {new CubeTuple(Side.TOP, SideOrientation.UPSIDE_DOWN, Side.FRONT), new ResultTuple(FrontToBottom, SideOrientation.UPSIDE_DOWN)},
            {new CubeTuple(Side.TOP, SideOrientation.UPSIDE_DOWN, Side.RIGHT), new ResultTuple(LeftToBottom, SideOrientation.UPSIDE_DOWN)},
            {new CubeTuple(Side.TOP, SideOrientation.UPSIDE_DOWN, Side.LEFT), new ResultTuple(RightToBottom, SideOrientation.UPSIDE_DOWN)},
            {new CubeTuple(Side.TOP, SideOrientation.UPSIDE_DOWN, Side.TOP), new ResultTuple(BottomToBottom, SideOrientation.UPSIDE_DOWN)},
            {new CubeTuple(Side.TOP, SideOrientation.UPSIDE_DOWN, Side.DOWN), new ResultTuple(TopToBottom, SideOrientation.UPSIDE_DOWN)},
            {new CubeTuple(Side.TOP, SideOrientation.UPSIDE_DOWN, Side.BACK), new ResultTuple(BackToBottom, SideOrientation.UPSIDE_DOWN)},

            {new CubeTuple(Side.TOP, SideOrientation.COUNTERCLOCKWISE, Side.FRONT), new ResultTuple(RightToBottom, SideOrientation.UPSIDE_DOWN)},
            {new CubeTuple(Side.TOP, SideOrientation.COUNTERCLOCKWISE, Side.RIGHT), new ResultTuple(FrontToBottom, SideOrientation.UPSIDE_DOWN)},
            {new CubeTuple(Side.TOP, SideOrientation.COUNTERCLOCKWISE, Side.LEFT), new ResultTuple(BackToBottom, SideOrientation.UPSIDE_DOWN)},
            {new CubeTuple(Side.TOP, SideOrientation.COUNTERCLOCKWISE, Side.TOP), new ResultTuple(BottomToBottom, SideOrientation.COUNTERCLOCKWISE)},
            {new CubeTuple(Side.TOP, SideOrientation.COUNTERCLOCKWISE, Side.DOWN), new ResultTuple(TopToBottom, SideOrientation.CLOCKWISE)},
            {new CubeTuple(Side.TOP, SideOrientation.COUNTERCLOCKWISE, Side.BACK), new ResultTuple(LeftToBottom, SideOrientation.UPSIDE_DOWN)},
                        
            //BOTTOM
            
            {new CubeTuple(Side.DOWN, SideOrientation.STRAIGHT, Side.FRONT), new ResultTuple(FrontToBottom, SideOrientation.STRAIGHT)},
            {new CubeTuple(Side.DOWN, SideOrientation.STRAIGHT, Side.RIGHT), new ResultTuple(RightToBottom, SideOrientation.STRAIGHT)},
            {new CubeTuple(Side.DOWN, SideOrientation.STRAIGHT, Side.LEFT), new ResultTuple(LeftToBottom, SideOrientation.STRAIGHT)},
            {new CubeTuple(Side.DOWN, SideOrientation.STRAIGHT, Side.TOP), new ResultTuple(TopToBottom, SideOrientation.STRAIGHT)},
            {new CubeTuple(Side.DOWN, SideOrientation.STRAIGHT, Side.DOWN), new ResultTuple(BottomToBottom, SideOrientation.STRAIGHT)},
            {new CubeTuple(Side.DOWN, SideOrientation.STRAIGHT, Side.BACK), new ResultTuple(BackToBottom, SideOrientation.STRAIGHT)},

            {new CubeTuple(Side.DOWN, SideOrientation.CLOCKWISE, Side.FRONT), new ResultTuple(RightToBottom, SideOrientation.STRAIGHT)},
            {new CubeTuple(Side.DOWN, SideOrientation.CLOCKWISE, Side.RIGHT), new ResultTuple(BackToBottom, SideOrientation.STRAIGHT)},
            {new CubeTuple(Side.DOWN, SideOrientation.CLOCKWISE, Side.LEFT), new ResultTuple(FrontToBottom, SideOrientation.STRAIGHT)},
            {new CubeTuple(Side.DOWN, SideOrientation.CLOCKWISE, Side.TOP), new ResultTuple(TopToBottom, SideOrientation.COUNTERCLOCKWISE)},
            {new CubeTuple(Side.DOWN, SideOrientation.CLOCKWISE, Side.DOWN), new ResultTuple(BottomToBottom, SideOrientation.CLOCKWISE)},
            {new CubeTuple(Side.DOWN, SideOrientation.CLOCKWISE, Side.BACK), new ResultTuple(LeftToBottom, SideOrientation.STRAIGHT)},

            {new CubeTuple(Side.DOWN, SideOrientation.UPSIDE_DOWN, Side.FRONT), new ResultTuple(BackToBottom, SideOrientation.STRAIGHT)},
            {new CubeTuple(Side.DOWN, SideOrientation.UPSIDE_DOWN, Side.RIGHT), new ResultTuple(LeftToBottom, SideOrientation.STRAIGHT)},
            {new CubeTuple(Side.DOWN, SideOrientation.UPSIDE_DOWN, Side.LEFT), new ResultTuple(RightToBottom, SideOrientation.STRAIGHT)},
            {new CubeTuple(Side.DOWN, SideOrientation.UPSIDE_DOWN, Side.TOP), new ResultTuple(TopToBottom, SideOrientation.UPSIDE_DOWN)},
            {new CubeTuple(Side.DOWN, SideOrientation.UPSIDE_DOWN, Side.DOWN), new ResultTuple(BottomToBottom, SideOrientation.UPSIDE_DOWN)},
            {new CubeTuple(Side.DOWN, SideOrientation.UPSIDE_DOWN, Side.BACK), new ResultTuple(FrontToBottom, SideOrientation.STRAIGHT)},

            {new CubeTuple(Side.DOWN, SideOrientation.COUNTERCLOCKWISE, Side.FRONT), new ResultTuple(LeftToBottom, SideOrientation.STRAIGHT)},
            {new CubeTuple(Side.DOWN, SideOrientation.COUNTERCLOCKWISE, Side.RIGHT), new ResultTuple(FrontToBottom, SideOrientation.STRAIGHT)},
            {new CubeTuple(Side.DOWN, SideOrientation.COUNTERCLOCKWISE, Side.LEFT), new ResultTuple(BackToBottom, SideOrientation.STRAIGHT)},
            {new CubeTuple(Side.DOWN, SideOrientation.COUNTERCLOCKWISE, Side.TOP), new ResultTuple(TopToBottom, SideOrientation.CLOCKWISE)},
            {new CubeTuple(Side.DOWN, SideOrientation.COUNTERCLOCKWISE, Side.DOWN), new ResultTuple(BottomToBottom, SideOrientation.COUNTERCLOCKWISE)},
            {new CubeTuple(Side.DOWN, SideOrientation.COUNTERCLOCKWISE, Side.BACK), new ResultTuple(RightToBottom, SideOrientation.STRAIGHT)},
                        
            //BACK
            
            {new CubeTuple(Side.BACK, SideOrientation.STRAIGHT, Side.FRONT), new ResultTuple(TopToBottom, SideOrientation.UPSIDE_DOWN)},
            {new CubeTuple(Side.BACK, SideOrientation.STRAIGHT, Side.RIGHT), new ResultTuple(LeftToBottom, SideOrientation.CLOCKWISE)},
            {new CubeTuple(Side.BACK, SideOrientation.STRAIGHT, Side.LEFT), new ResultTuple(RightToBottom, SideOrientation.COUNTERCLOCKWISE)},
            {new CubeTuple(Side.BACK, SideOrientation.STRAIGHT, Side.TOP), new ResultTuple(FrontToBottom, SideOrientation.UPSIDE_DOWN)},
            {new CubeTuple(Side.BACK, SideOrientation.STRAIGHT, Side.DOWN), new ResultTuple(BackToBottom, SideOrientation.STRAIGHT)},
            {new CubeTuple(Side.BACK, SideOrientation.STRAIGHT, Side.BACK), new ResultTuple(BottomToBottom, SideOrientation.STRAIGHT)},

            {new CubeTuple(Side.BACK, SideOrientation.CLOCKWISE, Side.FRONT), new ResultTuple(TopToBottom, SideOrientation.CLOCKWISE)},
            {new CubeTuple(Side.BACK, SideOrientation.CLOCKWISE, Side.RIGHT), new ResultTuple(FrontToBottom, SideOrientation.CLOCKWISE)},
            {new CubeTuple(Side.BACK, SideOrientation.CLOCKWISE, Side.LEFT), new ResultTuple(BackToBottom, SideOrientation.COUNTERCLOCKWISE)},
            {new CubeTuple(Side.BACK, SideOrientation.CLOCKWISE, Side.TOP), new ResultTuple(RightToBottom, SideOrientation.UPSIDE_DOWN)},
            {new CubeTuple(Side.BACK, SideOrientation.CLOCKWISE, Side.DOWN), new ResultTuple(LeftToBottom, SideOrientation.STRAIGHT)},
            {new CubeTuple(Side.BACK, SideOrientation.CLOCKWISE, Side.BACK), new ResultTuple(BottomToBottom, SideOrientation.CLOCKWISE)},

            {new CubeTuple(Side.BACK, SideOrientation.UPSIDE_DOWN, Side.FRONT), new ResultTuple(TopToBottom, SideOrientation.STRAIGHT)},
            {new CubeTuple(Side.BACK, SideOrientation.UPSIDE_DOWN, Side.RIGHT), new ResultTuple(RightToBottom, SideOrientation.CLOCKWISE)},
            {new CubeTuple(Side.BACK, SideOrientation.UPSIDE_DOWN, Side.LEFT), new ResultTuple(LeftToBottom, SideOrientation.COUNTERCLOCKWISE)},
            {new CubeTuple(Side.BACK, SideOrientation.UPSIDE_DOWN, Side.TOP), new ResultTuple(BackToBottom, SideOrientation.UPSIDE_DOWN)},
            {new CubeTuple(Side.BACK, SideOrientation.UPSIDE_DOWN, Side.DOWN), new ResultTuple(FrontToBottom, SideOrientation.STRAIGHT)},
            {new CubeTuple(Side.BACK, SideOrientation.UPSIDE_DOWN, Side.BACK), new ResultTuple(BottomToBottom, SideOrientation.UPSIDE_DOWN)},

            {new CubeTuple(Side.BACK, SideOrientation.COUNTERCLOCKWISE, Side.FRONT), new ResultTuple(TopToBottom, SideOrientation.COUNTERCLOCKWISE)},
            {new CubeTuple(Side.BACK, SideOrientation.COUNTERCLOCKWISE, Side.RIGHT), new ResultTuple(BackToBottom, SideOrientation.CLOCKWISE)},
            {new CubeTuple(Side.BACK, SideOrientation.COUNTERCLOCKWISE, Side.LEFT), new ResultTuple(FrontToBottom, SideOrientation.COUNTERCLOCKWISE)},
            {new CubeTuple(Side.BACK, SideOrientation.COUNTERCLOCKWISE, Side.TOP), new ResultTuple(LeftToBottom, SideOrientation.UPSIDE_DOWN)},
            {new CubeTuple(Side.BACK, SideOrientation.COUNTERCLOCKWISE, Side.DOWN), new ResultTuple(RightToBottom, SideOrientation.STRAIGHT)},
            {new CubeTuple(Side.BACK, SideOrientation.COUNTERCLOCKWISE, Side.BACK), new ResultTuple(BottomToBottom, SideOrientation.COUNTERCLOCKWISE)}
        };


        private static Dictionary<char, double> CommandDurations = new Dictionary<char, double>
        {
            {'A', 0.6 },
            {'a', 0.6 },
            {'Z', 1.2 },
            {'P', 0.3 },
            {'p', 0.3 },
            {'C', 0.3 },
            {'c', 0.3 },
            {'U', 2.2 }, // The actual time is 4.5s, however on average half the times this is called the platform is already elevated
            {'u', 4.5 }
        };

        private static double DELAY_BETWEEN_COMMANDS = 0.1;

        /// <summary>
        /// Calculates the duration, in seconds, that will take the robot to execute
        /// the series of commands in the provided string
        /// </summary>
        public static int GetRobotSolutionDuration(string commands)
        {
            // Create the 'F' listing in the dictionary
            if (!CommandDurations.ContainsKey('F'))
            {
                CommandDurations.Add('F', 0.0);
                CommandDurations['F'] = GetRobotSolutionDuration("PuCUpPuCUp");
            }

            double total = commands.Length * DELAY_BETWEEN_COMMANDS;

            foreach (KeyValuePair<char, double> k in CommandDurations)
                total += k.Value * (commands.Count((c) => { return c == k.Key; }));

            return (int)Math.Floor(total);
        }
    }
}
