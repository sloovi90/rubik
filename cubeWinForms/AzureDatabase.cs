﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Resources;
using System.Drawing;

namespace cubeWinForms
{
    class AzureDatabase
    {
        private const string CONNECTION_STRING = "Server=tcp:rubiks-db.database.windows.net,1433;" +
            "Database=rubiks-db;User ID=rubik@rubiks-db;Password=SolveMeBitch!;" +
            "Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";

        public static void GetColorTrainingData(out Color[] colors, out int[] cubeColors)
        {
            IList<Color> colorList = new List<Color>();
            IList<int> cubeColorsList = new List<int>();

            using (SqlConnection con = CreateConnection())
            {
                con.Open();

                SqlCommand cmd = con.CreateCommand();
                cmd.CommandText = "SELECT R,G,B,CubeColor FROM dbo.ColorTrainingData";
                SqlDataReader reader = cmd.ExecuteReader();

                while(reader.Read())
                {
                    colorList.Add(Color.FromArgb(reader.GetInt32(0), reader.GetInt32(1), reader.GetInt32(2)));
                    cubeColorsList.Add(reader.GetInt32(3));
                }

                reader.Close();
                colors = colorList.ToArray<Color>();
                cubeColors = cubeColorsList.ToArray<int>();
            }
        }

        public static void InsertColorTrainingData(IDictionary<Color, CubeColors> TrainData)
        {
            using (SqlConnection con = CreateConnection())
            {
                con.Open();
                using (SqlTransaction trans = con.BeginTransaction())
                {
                    SqlCommand cmd = con.CreateCommand();
                    cmd.Transaction = trans;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "INSERT INTO dbo.ColorTrainingData (R,G,B,CubeColor) VALUES(@R,@G,@B,@CubeColor)";
                    cmd.Parameters.Add("@R", SqlDbType.TinyInt);
                    cmd.Parameters.Add("@G", SqlDbType.TinyInt);
                    cmd.Parameters.Add("@B", SqlDbType.TinyInt);
                    cmd.Parameters.Add("@CubeColor", SqlDbType.TinyInt);
                    foreach (KeyValuePair<Color, CubeColors> color in TrainData)
                    {
                        cmd.Parameters["@R"].Value = color.Key.R;
                        cmd.Parameters["@G"].Value = color.Key.G;
                        cmd.Parameters["@B"].Value = color.Key.B;
                        cmd.Parameters["@CubeColor"].Value = (int)color.Value;
                        cmd.ExecuteNonQuery();
                        Console.Write("Inserted color");
                    }
                    trans.Commit();
                }
            }
        }

        // NOTE: Use carefully (this is also vulnerable code)
        public static void Truncate(string TableName)
        {
            using (SqlConnection con = CreateConnection())
            {
                con.Open();

                SqlCommand cmd = con.CreateCommand();
                cmd.CommandText = "TRUNCATE TABLE " + TableName;
                cmd.ExecuteNonQuery();
            }
        }

        private static SqlConnection CreateConnection()
        {
            return new SqlConnection(CONNECTION_STRING);
        }
    }
}
