﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cubeWinForms
{
    static class Utils
    {
        const string JAVA_JRE_PATH = @"C:\Program Files (x86)\Java\jre1.8.0_91\bin\java.exe";

        /// <summary>
        /// Sends the cube structure to the external algorithm executable and retrieves
        /// the calculated solution
        /// </summary>
        /// <param name="cubeString">The cube structure as understood by the external program</param>
        /// <returns>The cube solution</returns>
        public static string GetAlgorithmSolution(string cubeString)
        {
            Process p = new Process();
            p.StartInfo.Arguments = "-jar cubeSolver.jar " + cubeString;
            p.StartInfo.FileName = JAVA_JRE_PATH;
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.CreateNoWindow = true;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.RedirectStandardInput = true;
            p.StartInfo.RedirectStandardError = true;
            p.Start();
            string redirectedOutput = string.Empty;
            redirectedOutput += p.StandardOutput.ReadToEnd();
            return redirectedOutput;
        }
    }
}
