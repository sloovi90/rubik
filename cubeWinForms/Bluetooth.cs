﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using InTheHand.Net.Sockets;
using InTheHand.Net;
using InTheHand.Net.Bluetooth;
using System.IO;
using System.Net.Sockets;
using System.ComponentModel;

namespace cubeWinForms
{
    public class Bluetooth
    {
        const bool DEBUG_MODE = false;

        NetworkStream arduinoStream { get; set;  }
        BluetoothClient client { get; set; }

        public void Connect(string mac)
        {
            //string mac = "58:c3:8b:e5:1d:57"; //Bernie
            BluetoothAddress addr = BluetoothAddress.Parse(mac);
            Guid serviceClass;
            serviceClass = BluetoothService.SerialPort;
            var endpoint = new BluetoothEndPoint(addr, serviceClass);
            client = new BluetoothClient();
            client.Connect(endpoint);
            arduinoStream = client.GetStream();

            while (client.Available > 0)
                arduinoStream.ReadByte();
        }

        public void SendCommand(string command)
        {
            SendString(command);
        }

        public string SendCommandAndWait(string command)
        {
            if(DEBUG_MODE)
            {
                System.Threading.Thread.Sleep(500);
                return "";
            }

            int counter = 0;
            SendString(command);

            while (counter < command.Length)
            {
                if (ReceiveString() == "D")
                {
                    counter++;
                }
            }
            return command;
        }

        private void SendString(string msg)
        {
            byte[] msgArray = Encoding.ASCII.GetBytes(msg);
            arduinoStream.Write(msgArray, 0, msgArray.Length);
            arduinoStream.Flush();
        }

        private string ReceiveString()
        {
            if(!arduinoStream.CanRead)
            {
                Console.WriteLine("Connection is closed");
                return null;
            }

            while (client.Available <= 0) { }

            char b;
            while((b = (char)arduinoStream.ReadByte()) == -1) { }
            string s = Char.ToString(b);
            Console.Write(s);
            return s;
        }

        public void Close()
        {
            arduinoStream.Close();
            client.Close();
        }
    }
}