﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cubeWinForms
{
    public partial class ColorCalibration : Form
    {
        Capture capture;
        EventHandler ev;
        int index;
        Color[] colors;
        PictureBox[] colorBoxes;

        public ColorCalibration(int camID)
        {
            InitializeComponent();
            InitCapture(camID);
            index = 0;
            colors = new Color[6];
            colorBoxes = new PictureBox[6];
            colorBoxes[0] = pbRed;
            colorBoxes[1] = pbBlue;
            colorBoxes[2] = pbGreen;
            colorBoxes[3] = pbWhite;
            colorBoxes[4] = pbYellow;
            colorBoxes[5] = pbOrange;
        }

        private void ColorCalibration_Load(object sender, EventArgs e)
        {
            
        }

        private void InitCapture(int cam)
        {
            capture = new Capture(cam); //create a camera captue

            capture.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameHeight, 300);
            capture.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameWidth, 300);

            ev = new EventHandler(delegate (object sender, EventArgs e)
            {  //run this until application closed (close button click on image viewer)
                Image<Bgr, Byte> img = capture.QueryFrame().ToImage<Bgr, byte>();

                // Crop image from 16:9 to 1:1 from the center. NOTE: This assumes that the input image is 16:9
                img.ROI = new Rectangle(x: img.Width / 32 * 7, y: 0, width: img.Height, height: img.Height);
                // Resize to 400x400
                img = img.Resize(300, 300, Inter.Linear);
                pictureBox1.Image = img.Bitmap; //draw the image obtained from camera
            });
            Application.Idle += ev;
        }

        private void ProcessFrame(object sender, EventArgs arg)
        {
            pictureBox1.Image = capture.QueryFrame().Bitmap;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            button3.Enabled = true;
            Application.Idle -= ev;
            capture.Dispose();
            ToNextStep();

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            if(0 < index && index < 7)
            {
                Bitmap b = new Bitmap(pictureBox1.Image);
                colors[index - 1] = b.GetPixel(e.X, e.Y);
                colorBoxes[index - 1].BackColor = colors[index - 1];
                ToNextStep();
            }
        }

        private void ToNextStep()
        {
            if (index >= 6)
            {
                index = 7;
                button2.Enabled = true;
            }
            else
            {
                index++;
                label8.Top += 30;
            }

        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            IDictionary<Color, CubeColors> dict = new Dictionary<Color, CubeColors>();

            if (!colors[0].IsEmpty)
                dict.Add(colors[0], CubeColors.RED);
            if (!colors[1].IsEmpty)
                dict.Add(colors[1], CubeColors.BLUE);
            if (!colors[2].IsEmpty)
                dict.Add(colors[2], CubeColors.GREEN);
            if (!colors[3].IsEmpty)
                dict.Add(colors[3], CubeColors.WHITE);
            if (!colors[4].IsEmpty)
                dict.Add(colors[4], CubeColors.YELLOW);
            if (!colors[5].IsEmpty)
                dict.Add(colors[5], CubeColors.ORANGE);

            AzureDatabase.InsertColorTrainingData(dict);
            Close();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            ToNextStep();
        }

        private void ColorCalibration_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Idle -= ev;
            capture.Dispose();
        }
    }
}
