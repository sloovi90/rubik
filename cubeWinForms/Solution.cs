﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cubeWinForms
{
    public enum MoveDirection { CLOCKWISE, COUNTERCLOCKWISE, HALF_TURN };
    public class Solution
    {
        public List<Move> Moves { get; }
        private Cube Cube { get;  }

        public Solution(List<Move> moves)
        {
            this.Moves = moves;
        }

        public Solution(string solutionString, Cube cube)
        {
            Cube = cube;
            Moves = new List<Move>();

            foreach (string m in solutionString.Trim().Split(' '))
                Moves.Add(new Move(m));
        }

        public int Length { get { return Moves.Count; } }

        /// <summary>
        /// A generator for returning the moves one by one. Each call to the generator
        /// yields a tuple that holds the next move and the remaining number of moves
        /// until the solution is reached.
        /// </summary>
        /// <returns>The next move to perform and the number of remaining moves</returns>
        public IEnumerable<Tuple<Move, string, int>> MoveGenerator()
        {
            for (int i = 0; i < Moves.Count; i++)
                yield return new Tuple<Move, string, int>(Moves[i], SolutionToCommands(Moves.GetRange(i, Moves.Count - i)), Moves.Count - i - 1);
        }

        private string SolutionToCommands(List<Move> moves)
        {
            StringBuilder strBuilder = new StringBuilder();
            RobotProxy proxy = new RobotProxy(Cube, Side.DOWN, RobotProxy.SideOrientation.STRAIGHT, null);
            foreach(Move m in moves)
                strBuilder.Append(proxy.MakeMove(m));

            return strBuilder.ToString();
        }
    }

    public class Move
    {
        public Side Side { set; get; }
        public MoveDirection Direction { set; get; }

        public Move(Side side, MoveDirection direction)
        {
            this.Side = side;
            this.Direction = direction;
        }

        public Move(string moveString)
        {
            Side = Cube.CharToSide(moveString[0]);

            if (moveString.Length > 1)
            {
                if (moveString[1] == '2')
                    Direction = MoveDirection.HALF_TURN;
                else if (moveString[1] == '\'')
                    Direction = MoveDirection.COUNTERCLOCKWISE;
                else
                    throw new ArgumentException("Could not parse solution; " + moveString[1] +
                        " in " + moveString + " is not a valid move direction");
            }
            else
                Direction = MoveDirection.CLOCKWISE;
        }

        public override string ToString()
        {
            string s = "";
            switch (Side)
            {
                case Side.TOP:    s = "U"; break;
                case Side.FRONT:  s = "F"; break;
                case Side.BACK:   s = "B"; break;
                case Side.DOWN: s = "D"; break;
                case Side.RIGHT:  s = "R"; break;
                case Side.LEFT:   s = "L"; break;
            }

            switch(Direction)
            {
                case MoveDirection.COUNTERCLOCKWISE: s += "'"; break;
                case MoveDirection.HALF_TURN: s += "2"; break;
            }

            return s;
        }
    }
}
