﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cubeWinForms
{
    public enum CubeColors { WHITE = 0, RED, ORANGE, BLUE, YELLOW, GREEN };
    public enum Side { TOP = 0, RIGHT, FRONT, DOWN, LEFT, BACK };


    public class Face
    {
        //0 1 2
        //3 4 5
        //6 7 8
        public CubeColors[] face;

        public Face() { }

        public Face(CubeColors[] cc)
        {
            face = new CubeColors[9];
            cc.CopyTo(face, 0);
        }

        public CubeColors FaceColor
        {
            get { return face[4]; }
        }

        public void SetRow(int i, CubeColors[] row)
        {
            for (int j = 0; j < 3; ++j)
                face[i * 3 + j] = row[j];
        }

        public void SetColumn(int j, CubeColors[] col)
        {
            for (int i = 0; i < 3; ++i)
                face[i * 3 + j] = col[i];
        }

        /// <summary>
        /// Rotate the face 90 degrees clockwise
        /// </summary>
        public void RotateClockwise()
        {
            CubeColors[] c = new CubeColors[9];
            c[2] = face[0];
            c[5] = face[1];
            c[8] = face[2];
            c[1] = face[3];
            c[4] = face[4];
            c[7] = face[5];
            c[0] = face[6];
            c[3] = face[7];
            c[6] = face[8];
            face = c;
        }

        /// <summary>
        /// Rotate the face 90 degrees counterclockwise
        /// </summary>
        public void RotateCounterclockwise()
        {
            CubeColors[] c = new CubeColors[9];
            c[0] = face[2];
            c[1] = face[5];
            c[2] = face[8];
            c[3] = face[1];
            c[4] = face[4];
            c[5] = face[7];
            c[6] = face[0];
            c[7] = face[3];
            c[8] = face[6];
            face = c;
        }

        /// <summary>
        /// Returns the color at square (i, j). The coordinate is 0-based, i.e (0, 0) marks the top-left square
        /// </summary>
        /// <param name="i">The row of the requested square</param>
        /// <param name="j">The column of the requested square</param>
        /// <returns></returns>
        public CubeColors ColorAt(int i, int j)
        {
            return face[i * 3 + j];
        }

        public bool IsSolved()
        {
            for (int i = 0; i < 9; i++)
                if (face[i] != FaceColor)
                    return false;
            return true;
        }
    };

    
    public class Cube
    {
        private string[] CUBE_FACES_ORDER = { "U", "R", "F", "D", "L", "B" };

        private Dictionary<Side, Face> faces;

        public Cube()
        {
            faces = new Dictionary<Side, Face>();
        }

        public void SetFaceBySide(Side side, Face face)
        {
            if (faces.ContainsKey(side))
                faces[side] = face;
            else
                faces.Add(side, face);
            //faces[Side] = face;
        }

        public Face GetFaceBySide(Side side)
        {
            try
            {
                return faces[side];
            } catch(KeyNotFoundException e)
            {
                return null;
            }
        }

        /// <summary>
        /// Returns the string representing the cube as understood by the external Kociemba algorithm JAR
        /// </summary>
        public string GetCubeString()
        {
            string str = "";
            CubeColors[] cArr = new CubeColors[6];
            for (Side i = Side.TOP; i <= Side.BACK; ++i)
                cArr[(int)i] = GetFaceBySide(i).FaceColor;
            foreach (Side s in Enum.GetValues(typeof(Side)))
            {
                for (int i = 0; i < 3; ++i)
                    for (int j = 0; j < 3; ++j)
                    {
                        CubeColors c = GetFaceBySide(s).ColorAt(i, j);
                        for (int k = 0; k < 6; ++k)
                        {
                            if (cArr[k] == c)
                                str += CUBE_FACES_ORDER[k];

                        }
                    }
            }
            return str;
        }

        /// <summary>
        /// Builds the cube from the given string representing the cube as understood by the external Kociemba algorithm JAR
        /// </summary>
        public void BuildCubeFromString(string str)
        {
            if (str.Length != 54)
                throw new ArgumentException("Cube string was " + str.Length.ToString() + " characters long, but 54 was expected");

            // Map the indices to the corresponding colors
            str = str.Replace('F', '0').Replace('L', '1').Replace('B', '2').Replace('D', '3').Replace('R', '4').Replace('U', '5');

            foreach (Side s in Enum.GetValues(typeof(Side)))
            {
                CubeColors[] cArr = new CubeColors[9];
                for (int i = 0; i < 9; ++i)
                    cArr[i] = (CubeColors)(int.Parse(str[9 * (int)s + i].ToString()));
                Face f = new Face(cArr);
                SetFaceBySide(s, f);
            }
        }

        public void MakeMove(Move move)
        {
            RotateSide(move.Side, move.Direction != MoveDirection.COUNTERCLOCKWISE);

            // Rotate twice if HALF_TURN
            if(move.Direction == MoveDirection.HALF_TURN)
            {
                RotateSide(move.Side, true);
            }
        }

        private void rotateFront(bool clockWise)
        {
            CubeColors[] bottomSide = new CubeColors[3], 
                leftSide = new CubeColors[3], 
                rightSide = new CubeColors[3], 
                topSide = new CubeColors[3];

            for (int i = 0; i < 3; ++i)
            {
                topSide[i] = faces[Side.TOP].ColorAt(2, i);
                leftSide[i] = faces[Side.LEFT].ColorAt(i, 2);
                rightSide[i] = faces[Side.RIGHT].ColorAt(i, 0);
                bottomSide[i] = faces[Side.DOWN].ColorAt(0, i);
            }

            if (clockWise)
            {
                faces[Side.FRONT].RotateClockwise();
                Array.Reverse(leftSide);
                faces[Side.TOP].SetRow(2, leftSide);
                faces[Side.LEFT].SetColumn(2, bottomSide);
                faces[Side.RIGHT].SetColumn(0, topSide);
                Array.Reverse(rightSide);
                faces[Side.DOWN].SetRow(0, rightSide);
            }
            else
            {
                faces[Side.FRONT].RotateCounterclockwise();
                faces[Side.TOP].SetRow(2, rightSide);
                Array.Reverse(topSide);
                faces[Side.LEFT].SetColumn(2, topSide);
                Array.Reverse(bottomSide);
                faces[Side.RIGHT].SetColumn(0, bottomSide);
                faces[Side.DOWN].SetRow(0, leftSide);
            }
        }

        private void rotateTop(bool clockWise)
        {
            CubeColors[] frontSide = new CubeColors[3], 
                leftSide = new CubeColors[3], 
                rightSide = new CubeColors[3], 
                backSide = new CubeColors[3];
            for (int i = 0; i < 3; ++i)
            {
                frontSide[i] = faces[Side.FRONT].ColorAt(0, i);
                leftSide[i] = faces[Side.LEFT].ColorAt(0, i);
                rightSide[i] = faces[Side.RIGHT].ColorAt(0, i);
                backSide[i] = faces[Side.BACK].ColorAt(0, i);
            }
            if (clockWise)
            {
                faces[Side.TOP].RotateClockwise();
                faces[Side.BACK].SetRow(0, leftSide);
                faces[Side.LEFT].SetRow(0, frontSide);
                faces[Side.RIGHT].SetRow(0, backSide);
                //Array.Reverse(rightSide);
                faces[Side.FRONT].SetRow(0, rightSide);
            }
            else
            {
                faces[Side.TOP].RotateCounterclockwise();
                faces[Side.BACK].SetRow(0, rightSide);
                faces[Side.LEFT].SetRow(0, backSide);
                // Array.Reverse(frontSide);
                faces[Side.RIGHT].SetRow(0, frontSide);
                faces[Side.FRONT].SetRow(0, leftSide);
            }

        }
        private void rotateBottom(bool clockWise)
        {
            CubeColors[] backSide = new CubeColors[3], leftSide = new CubeColors[3], rightSide = new CubeColors[3], frontSide = new CubeColors[3];
            for (int i = 0; i < 3; ++i)
            {
                backSide[i] = faces[Side.BACK].ColorAt(2, i);
                leftSide[i] = faces[Side.LEFT].ColorAt(2, i);
                rightSide[i] = faces[Side.RIGHT].ColorAt(2, i);
                frontSide[i] = faces[Side.FRONT].ColorAt(2, i);
            }
            if (clockWise)
            {
                faces[Side.DOWN].RotateClockwise();
                faces[Side.FRONT].SetRow(2, leftSide);
                faces[Side.LEFT].SetRow(2, backSide);
                faces[Side.RIGHT].SetRow(2, frontSide);
                faces[Side.BACK].SetRow(2, rightSide);

            }
            else
            {
                faces[Side.DOWN].RotateCounterclockwise();
                faces[Side.FRONT].SetRow(2, rightSide);
                faces[Side.LEFT].SetRow(2, frontSide);
                faces[Side.RIGHT].SetRow(2, backSide);
                faces[Side.BACK].SetRow(2, leftSide);
            }
        }
        private void rotateBack(bool clockWise)
        {
            CubeColors[] topSide = new CubeColors[3], leftSide = new CubeColors[3], rightSide = new CubeColors[3], bottomSide = new CubeColors[3];
            for (int i = 0; i < 3; ++i)
            {
                topSide[i] = faces[Side.TOP].ColorAt(0, i);
                leftSide[i] = faces[Side.LEFT].ColorAt(i, 0);
                rightSide[i] = faces[Side.RIGHT].ColorAt(i, 2);
                bottomSide[i] = faces[Side.DOWN].ColorAt(2, i);
            }
            if (clockWise)
            {
                faces[Side.BACK].RotateClockwise();
                Array.Reverse(topSide);
                faces[Side.TOP].SetRow(0, rightSide);
                faces[Side.LEFT].SetColumn(0, topSide);
                Array.Reverse(bottomSide);
                faces[Side.RIGHT].SetColumn(2, bottomSide);
                faces[Side.DOWN].SetRow(2, leftSide);
            }
            else
            {
                faces[Side.BACK].RotateCounterclockwise();
                Array.Reverse(leftSide);
                faces[Side.TOP].SetRow(0, leftSide);
                faces[Side.LEFT].SetColumn(0, bottomSide);
                Array.Reverse(rightSide);
                faces[Side.RIGHT].SetColumn(2, topSide);
                faces[Side.DOWN].SetRow(2, rightSide);
            }
        }
        private void rotateRight(bool clockWise)
        {
            CubeColors[] bottomSide = new CubeColors[3], frontSide = new CubeColors[3], backSide = new CubeColors[3], topSide = new CubeColors[3];
            for (int i = 0; i < 3; ++i)
            {
                topSide[i] = faces[Side.TOP].ColorAt(i, 2);
                frontSide[i] = faces[Side.FRONT].ColorAt(i, 2);
                backSide[i] = faces[Side.BACK].ColorAt(i, 0);
                bottomSide[i] = faces[Side.DOWN].ColorAt(i, 2);
            }
            if (clockWise)
            {
                faces[Side.RIGHT].RotateClockwise();
                Array.Reverse(topSide);
                faces[Side.BACK].SetColumn(0, topSide);
                faces[Side.FRONT].SetColumn(2, bottomSide);
                faces[Side.TOP].SetColumn(2, frontSide);
                Array.Reverse(backSide);
                faces[Side.DOWN].SetColumn(2, backSide);
            }
            else
            {
                faces[Side.RIGHT].RotateCounterclockwise();
                Array.Reverse(backSide);
                faces[Side.TOP].SetColumn(2, backSide);
                faces[Side.FRONT].SetColumn(2, topSide);
                Array.Reverse(bottomSide);
                faces[Side.DOWN].SetColumn(2, frontSide);
                faces[Side.BACK].SetColumn(0, bottomSide);
            }
        }
        private void rotateLeft(bool clockWise)
        {
            CubeColors[] bottomSide = new CubeColors[3], frontSide = new CubeColors[3], backSide = new CubeColors[3], topSide = new CubeColors[3];
            for (int i = 0; i < 3; ++i)
            {
                topSide[i] = faces[Side.TOP].ColorAt(i, 0);
                frontSide[i] = faces[Side.FRONT].ColorAt(i, 0);
                backSide[i] = faces[Side.BACK].ColorAt(i, 2);
                bottomSide[i] = faces[Side.DOWN].ColorAt(i, 0);
            }
            if (clockWise)
            {
                faces[Side.LEFT].RotateClockwise();
                Array.Reverse(bottomSide);
                faces[Side.BACK].SetColumn(2, bottomSide);
                faces[Side.FRONT].SetColumn(0, topSide);
                Array.Reverse(backSide);
                faces[Side.TOP].SetColumn(0, backSide);
                faces[Side.DOWN].SetColumn(0, frontSide);
            }
            else
            {
                faces[Side.LEFT].RotateCounterclockwise();
                faces[Side.TOP].SetColumn(0, frontSide);
                faces[Side.FRONT].SetColumn(0, bottomSide);
                Array.Reverse(backSide);
                faces[Side.DOWN].SetColumn(0, backSide);
                Array.Reverse(topSide);
                faces[Side.BACK].SetColumn(2, topSide);
            }
        }

        public void RotateSide(Side side, bool clockwise)
        {
            switch (side)
            {
                case Side.FRONT:
                    rotateFront(clockwise);
                    break;
                case Side.TOP:
                    rotateTop(clockwise);
                    break;
                case Side.DOWN:
                    rotateBottom(clockwise);
                    break;
                case Side.BACK:
                    rotateBack(clockwise);
                    break;
                case Side.RIGHT:
                    rotateRight(clockwise);
                    break;
                case Side.LEFT:
                    rotateLeft(clockwise);
                    break;
                default:
                    break;
            }
        }

        /*public bool isSolved()
        {
            bool isSolved = true;
            for (int i = 0; i < 6; i++)
                if (!faces[i].IsSolved())
                    isSolved = false;

            return isSolved;
        }*/

        public static Side CharToSide(char c)
        {
            switch (c)
            {
                case 'F':
                    return Side.FRONT;
                case 'D':
                    return Side.DOWN;
                case 'B':
                    return Side.BACK;
                case 'R':
                    return Side.RIGHT;
                case 'U':
                    return Side.TOP;
                case 'L':
                    return Side.LEFT;
                default:
                    throw new ArgumentException(c.ToString() + " is not a valid side");
            }
        }


    };
}
