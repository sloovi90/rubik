﻿using Accord.MachineLearning;
using Accord.MachineLearning.DecisionTrees.Learning;
using Accord.Math;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cubeWinForms
{
    class ColorProvider
    {
        private const int K = 5;
        KNearestNeighbors<Color> knn;

        public ColorProvider()
        {
            TrainModel();
        }

        public CubeColors GetCubeColor(Color c)
        {
            return (CubeColors)knn.Compute(c);
        }

        public CubeColors GetCubeColor(Bgr bgr)
        {
            return GetCubeColor(Color.FromArgb(
                Convert.ToInt32(bgr.Red),
                Convert.ToInt32(bgr.Green),
                Convert.ToInt32(bgr.Blue)
                ));
        }

        private void TrainModel()
        {
            Color[] inputs;
            int[] outputs;
            LoadTrainingData(out inputs, out outputs);

            knn = new KNearestNeighbors<Color>(
                k: K, inputs: inputs, outputs: outputs, distance: ColorDistanceSquared);

        }

        private void LoadTrainingData(out Color[] inputs, out int[] outputs)
        {
            AzureDatabase.GetColorTrainingData(out inputs, out outputs);
        }

        private double ColorDistanceSquared(Color x, Color y)
        {
            return Math.Pow(x.R - y.R, 2) + Math.Pow(x.G - y.G, 2) + Math.Pow(x.B - y.B, 2);
        }
    }
}
