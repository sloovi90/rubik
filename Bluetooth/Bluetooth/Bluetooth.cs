﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using InTheHand.Net.Sockets;
using InTheHand.Net;
using InTheHand.Net.Bluetooth;
using System.IO;

namespace cubeWinForms
{
    class Bluetooth
    {
        public Stream createConnection()
        {
            //string mac = "98:d3:31:40:63:1b"; //
            string mac = "58:c3:8b:e5:1d:57"; //Bernie
          //  string mac = "48:59:29:95:a5:b7";
            BluetoothAddress addr = BluetoothAddress.Parse(mac);
            Guid serviceClass;
            serviceClass = BluetoothService.SerialPort;
            var endpoint = new BluetoothEndPoint(addr, serviceClass);
            var client = new BluetoothClient();
            client.Connect(endpoint);
            Stream peerStream = client.GetStream();
            return peerStream;
        }
        public void sendMessageToArduino(string msg)
        {
            Stream arduinoStream = createConnection();
            byte[] msgArray = Encoding.ASCII.GetBytes(msg);
            arduinoStream.Write(msgArray, 0, msgArray.Length);
            arduinoStream.Flush();
        }
    }
}